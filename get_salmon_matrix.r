##remember to module load R/3.3.3-shlib on shark...
if(!require(dplyr)) stop('error loading dplyr')
if(!require(readr)) stop('erro loading readr')
if(!require(rtracklayer)) stop('error loading rtracklayer')
if(!require(ggplot2)) stop('error loading ggplot2')
if(!require(tximport)) stop('error loading tximport')


args = commandArgs(trailingOnly = T)
if(length(args)!= 1) stop('usage: Rscript get_salmon_matrix.r configFile')
configFile=args[1]
## set before running
salmon_root_dir = system(command = paste0("cat ",configFile," | grep -w 'SALMON_OUTPUT_DIR' | cut -d '=' -f2"),intern = T)
output_dir=salmon_root_dir
gtfFile = system(command = paste0("cat ",configFile," | grep -w 'SALMON_GFF_FILE' | cut -d '=' -f2"),intern = T)
##make tx2gene
gtf = read_tsv(gtfFile,comment = '#',col_names = F)
colnames(gtf) = c('chr',
                  'source',
                  'type',
                  'start',
                  'end',
                  'score(not used)',
                  'strand',
                  'genomic_phase',
                  'additional_info')

gene_gtf = filter(gtf, type == 'gene')
tx_gtf = filter(gtf, type == 'transcript')
##parse additional info to get gene name id
tokenize_txgtf_additional_info = function(e) {
  tmp = lapply(strsplit(e,split=';'),trimws)
  geneid = unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^gene_id', tmp_e)]))
  txid = unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^transcript_id', tmp_e)]))
  genename= unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^gene_name', tmp_e)]))
  genetype= unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^gene_type', tmp_e)]))
  txname= unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^transcript_name', tmp_e)]))
  txtype= unlist(lapply(tmp, function(tmp_e) tmp_e[grepl('^transcript_type', tmp_e)]))
  if(!all(c(length(geneid) , length(genename) , length(genetype)) == length(e))) stop('fields(id, name or type) missing')
  tmp = data.frame(gene_id=trimws(gsub('^gene_id|"','',geneid)),
                   transcript_id=trimws(gsub('^transcript_id|"','',txid)),
                   gene_name=trimws(gsub('^gene_name|"','',genename)),
                   gene_type=trimws(gsub('^gene_type|"','',genetype)),
                   transcript_name=trimws(gsub('^transcript_name|"','',txname)),
                   transcript_type=trimws(gsub('^transcript_type|"','',txtype))
                   )
  tmp
}
tx_additional_dat = tokenize_txgtf_additional_info(tx_gtf$additional_info)
tx2gene = select(tx_additional_dat,
                 transcript_id,
                 gene_id)

##sample id that wiil be used
##dir name to sample id
sub_dirs = list.dirs(salmon_root_dir,recursive=F,full.names = T)
files = file.path(sub_dirs,'quant.sf')
sampleNames=basename(sub_dirs)

txi = tximport(files, type = 'salmon', tx2gene = tx2gene)
geneIDNametbl = (select(tx_additional_dat,gene_name,gene_id,gene_type)) %>% unique
pos = match(rownames(txi$abundance),geneIDNametbl$gene_id)
tx_tpm = data.frame(geneIDNametbl[pos,],txi$abundance); colnames(tx_tpm) = c('Name','ID','Type',sampleNames)

countMtr = tx_tpm
pcg_countMtr = filter(countMtr, Type == 'protein_coding')
nonpcg_countMtr = filter(countMtr, Type != 'protein_coding')

write_tsv(pcg_countMtr, path=file.path(output_dir,'protein_coding_gene_countMatrix.tsv'))
write_tsv(nonpcg_countMtr, path=file.path(output_dir,'Non_protein_coding_gene_countMatrix.tsv'))


##want a correlation heatmap plot?
gene.df = select(pcg_countMtr, Name,ID)
dat.df = select(pcg_countMtr, -Name, -ID,-Type) 
##remove zero
low.idx = rowSums(dat.df) == 0
dat.df = dat.df[!low.idx,]
gene.df = gene.df[!low.idx,]
corrSamples = cor(dat.df,method = 'pearson')
library(pheatmap)
library(RColorBrewer)
pheatmap(mat=corrSamples,
         ##          color = colorRampPalette((brewer.pal(n = 7, name = "RdYlBu")))(100),
         ##         cluster_cols=F,
         ##         show_colnames = F,
         border_color = 'white',
         show_colnames=F,
         filename=file.path(output_dir,'sampleCorrelation.heatmap.pdf'))

