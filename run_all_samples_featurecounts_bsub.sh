####################################################
# featureCounts in subread
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/bash

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    echo "$c"
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create featurecounts_bam_sample_scripts subdir in $SCRIPTS_PATH..."
    featurecounts_sample_scripts_path=${SCRIPTS_PATH}/featurecounts_sample_scripts
    mkdir -p ${featurecounts_sample_scripts_path}
    BAM_LIST=$(cat $configFile | grep -w 'FEATURECOUNTS_BAM_LIST' | cut -d '=' -f2)
    NCORE=$(cat $configFile | grep -w 'FEATURECOUNTS_NTHREADS' | cut -d '=' -f2)
    echo "looping through each bam file..."
    cat $BAM_LIST | while read line; do
        echo "$line"
        BAM=`basename $line`
        BAM=${BAM/.bam/}
                echo "#BSUB -J featurect_${BAM}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/featurect_${BAM}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/featurect_${BAM}.e 
#BSUB -q medium
#BSUB -n ${NCORE}
#BSUB -M 65536
#BSUB -R rusage[mem=65536]
#BSUB -B
#BSUB -N

echo \"submitting featurect_${BAM} jobs\"
module load subread/1.5.2

sh ${SCRIPTS_PATH}/featurecounts_for_one_sample.sh $line ${SCRIPTS_PATH}/config/tool_info.txt " > ${featurecounts_sample_scripts_path}/featurecounts_bam_${BAM}.lsf
                bsub< ${featurecounts_sample_scripts_path}/featurecounts_bam_${BAM}.lsf
                sleep 1
    done
fi
