####################################################
# rsem+star pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by mirdeep2 and bowtie)
####################################################

if [ $# != 2 ]; then
    echo " usage: sampleName configFile";
else
    sample=$1
    configFile=$2

    output_dir=$(cat $configFile | grep -w 'FASTQC_AFTER_CUTADAPT_OUTPUT_PATH' | cut -d '=' -f2)
    input_dir=$(cat $configFile | grep -w 'FASTQC_AFTER_CUTADAPT_INPUT_PATH' | cut -d '=' -f2)

    fastq1=${sample}/${sample}.cutadapt.fq
	
    output_dir1=${output_dir}/${sample}

	mkdir -p $output_dir1
	cd $output_dir1

	################fastQC###############################
	fastqc_path=$(cat $configFile | grep -w 'FASTQC_PATH' | cut -d '=' -f2)

##	echo "run fastQC on data before adaptor clipping..."
	$fastqc_path/fastqc -o $output_dir1 $input_dir/$fastq1 2>&1 |tee $output_dir1/${sample}.fastqc.log

fi
