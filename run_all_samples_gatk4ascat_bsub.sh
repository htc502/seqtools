####################################################
# gatk for ascat: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create gatk4ascat_sample_scripts subdir..."
    gatk4ascat_sample_scripts_path=${SCRIPTS_PATH}/gatk4ascat_sample_scripts
    mkdir -p $gatk4ascat_sample_scripts_path

    BAM_PAIR_LIST=$(cat $configFile | grep -w 'GATK4ASCAT_BAM_PAIR_LIST_FILE' | cut -d '=' -f2)

    cat ${BAM_PAIR_LIST} | while read line; do
        fields=( $line )
        sampleID=${fields[0]}
        tbam=${fields[1]}
        nbam=${fields[2]}
	echo "$sampleID"
        echo "#BSUB -J ${sampleID}_gatk4ascat
#BSUB -W 6:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${sampleID}_gatk4ascat.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${sampleID}_gatk4ascat.e 
#BSUB -q medium
#BSUB -n 10
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N

module load jdk
module load gatk/3.7

sh ${SCRIPTS_PATH}/gatk4ascat_for_one_sample.sh $tbam $nbam  $sampleID ${SCRIPTS_PATH}/config/tool_info.txt" >  ${gatk4ascat_sample_scripts_path}/gatk4ascat_${sampleID}.lsf
        bsub <  ${gatk4ascat_sample_scripts_path}/gatk4ascat_${sampleID}.lsf 
        sleep 1
    done

fi
