####################################################
# polysolver pipeline for one sample
# inputs: 
#    bamfile 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: sampleName configFile";
else
    samplefilepath=$1
    configFile=$2

	################polysolver###############################
  output_dir=$(cat $configFile | grep -w 'POLYSOLVER_OUTPUT_PATH' | cut -d '=' -f2)
  nthreads=$(cat $configFile | grep -w 'POLYSOLVER_NTHREADS' | cut -d '=' -f2)

  bamfile=$samplefilepath
  samplename=`basename $bamfile`
  samplename=${samplename/.bam/}
	
  output_dir1=${output_dir}/${samplename}

	mkdir -p $output_dir1
	cd $output_dir1

  echo "  $PSHOME/scripts/shell_call_hla_type $bamfile Unknown 1 hg19 STDFQ 0 $output_dir1"
  ##be sure that polysolver has been module loaded
  . /risapps/rhel6/polysolver/1.0/scripts/config.bash
  export NUM_THREADS=$nthreads ##overload the config.bash specification
  shell_call_hla_type $bamfile  Unknown   1  hg19  STDFQ  0  $output_dir1/polysolver

fi
