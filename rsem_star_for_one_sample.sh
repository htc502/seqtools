####################################################
# rsem with star for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
    fq1=$1
    fq2=$2
    configFile=$3
    refDir=$(cat $configFile | grep -w 'RSEMSTAR_REF_PREF' | cut -d '=' -f2)
    star_module_version=$(cat $configFile | grep -w 'STAR_MODULE_VERSION' | cut -d '=' -f2)
    rsem_module_version=$(cat $configFile | grep -w 'RSEM_MODULE_VERSION' | cut -d '=' -f2)
    outputDir=$(cat $configFile | grep -w 'RSEMSTAR_OUTPUT_PATH' | cut -d '=' -f2)
    np=$(cat $configFile | grep -w 'RSEMSTAR_NTHREAD' | cut -d '=' -f2)

    module load ${star_module_version}
    module load ${rsem_module_version}
    starpath=$(which STAR)
    starpath1=${starpath/%\/STAR/}

    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R1*gz/}

    rsem-calculate-expression  --paired-end \
                              --star \
                              --star-path $starpath1 \
                              --star-gzipped-read-file \
                              -p $np \
                              $fq1 \
                              $fq2 \
                              $refDir \
                              ${outputDir}/${samplename}
fi
