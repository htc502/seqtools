wd='/data/ghan/get_lncRNA_from_array/blast/data/rnaseqData/sra/data/star_mapping'
setwd(wd)
d=list.dirs('.',recursive=F,full.names=F)
dat <- list()
for( di in d) {
	dati <- read.delim(paste0(di,'/',di,'_rsem_including_star.genes.results'))
	rpkm <- dati[,7]
	dat[[length(dat)+1]] <- rpkm
}
dat1 <- do.call(cbind,dat)

rownames(dat1) <- dati[,1]
colnames(dat1) <- gsub('\\.fastq','',d)
write.csv(dat1,file='/data/ghan/get_lncRNA_from_array/blast/data/rnaseqData/sra/star_mapping_scripts/data_rpkm.csv')
	
