####################################################
# htseqcount pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: sampleName configFile";
else
    samplefilepath=$1
    configFile=$2

    ################htseqcount###############################
    output_dir=$(cat $configFile | grep -w 'HTSEQCOUNT_OUTPUT_PATH' | cut -d '=' -f2)
    gff_file=$(cat $configFile | grep -w 'HTSEQCOUNT_GFF_FILE' | cut -d '=' -f2)
    strand=$(cat $configFile | grep -w 'HTSEQCOUNT_STRAND' | cut -d '=' -f2)
    mmode=$(cat $configFile | grep -w 'HTSEQCOUNT_MODE' | cut -d '=' -f2)
    rorder=$(cat $configFile | grep -w 'HTSEQCOUNT_ORDER' | cut -d '=' -f2)
    buffer_reads=$(cat $configFile | grep -w 'HTSEQCOUNT_MAX_READS_IN_BUFFER' | cut -d '=' -f2)
    #  nthreads=$(cat $configFile | grep -w 'HTSEQCOUNT_NTHREAD' | cut -d '=' -f2)


    bamfile=$samplefilepath
    samplename=`basename $bamfile`
    samplename=${samplename/.bam/}
    
    output_dir1=${output_dir}/${samplename}

    mkdir -p $output_dir1
    cd $output_dir1

    echo "run htseq-count on data ..."
    echo "samtools view -F 4 $bamfile | htseq-count -s $strand -m $mmode --max-reads-in-buffer $buffer_reads -r $rorder - $gff_file >$output_dir1/$samplename.readsCount 2> $output_dir1/${samplename}.htseqcount.error.log"
    samtools view -F 4 $bamfile | htseq-count -s $strand -m $mmode --max-reads-in-buffer $buffer_reads -r $rorder - $gff_file >$output_dir1/$samplename.readsCount 2> $output_dir1/${samplename}.htseqcount.error.log

fi
