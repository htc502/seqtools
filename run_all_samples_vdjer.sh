####################################################
# vdjer pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create vdjer_mapping_sample_scripts subdir..."
    vdjer_mapping_sample_scripts_path=${SCRIPTS_PATH}/vdjer_mapping_sample_scripts
    mkdir -p $vdjer_mapping_sample_scripts_path

    ##vdjer will only do QC on those samples which undergo cutadapt...
    BAM_LIST=$(cat $configFile | grep -w 'VDJER_BAM_LIST_FILE' | cut -d '=' -f2)

    cat ${BAM_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        bam=$line
        samplename=`basename $bam`
        samplename=${samplename/.bam/}
        njob=4
        while [ " " == " " ]
        do
            d=`ps -aux | grep 'vdjer_for_one_sample' | wc -l`
            #echo "line=$line"
            if [ $d -le $njob ]
            then
                echo "$c=$line"
                echo "-------------------------------"
                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh ${SCRIPTS_PATH}/vdjer_for_one_sample.sh $bam IGH ${SCRIPTS_PATH}/config/tool_info.txt" >  ${vdjer_mapping_sample_scripts_path}/vdjer_IGH_${samplename}.sh
                chmod 755  ${vdjer_mapping_sample_scripts_path}/vdjer_IGH_${samplename}.sh
                ##                sh  ${vdjer_mapping_sample_scripts_path}/vdjer_IGH_${samplename}.sh &

                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh ${SCRIPTS_PATH}/vdjer_for_one_sample.sh $bam IGK ${SCRIPTS_PATH}/config/tool_info.txt" >  ${vdjer_mapping_sample_scripts_path}/vdjer_IGK_${samplename}.sh

                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh ${SCRIPTS_PATH}/vdjer_for_one_sample.sh $bam IGL ${SCRIPTS_PATH}/config/tool_info.txt" >  ${vdjer_mapping_sample_scripts_path}/vdjer_IGL_${samplename}.sh
                sleep 2
                break
            else
                sleep 30
            fi
        done
        echo "end"
    done

fi
