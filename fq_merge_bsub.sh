####################################################
# fq merge for pipeline
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1

	################fastQC###############################
  scripts_dir=$(cat $configFile | grep -w 'FQ_MERGE_SCRIPT_DIR' | cut -d '=' -f2)
  mkdir -p ${scripts_dir}

  input_table=$(cat $configFile | grep -w 'FQ_MERGE_INPUT_TABLE' | cut -d '=' -f2)
  output_dir=$(cat $configFile | grep -w 'FQ_MERGE_OUTPUT_PATH' | cut -d '=' -f2)

	mkdir -p $output_dir
	cd $output_dir

	echo "run fq merge..."
  targetFqFiles=( $(cut -f 1 $input_table | tail -n +2 |sort| uniq) )
  for tfile in ${targetFqFiles[@]}
  do
      fqR1=( $(grep  -P "^$tfile\t" $input_table | cut -f2  ) )
      echo "$tfile: number of fq R1 to merge: ${#fqR1[@]}"

      fqR2=( $(grep  -P "^$tfile\t" $input_table | cut -f3  ) )
      echo "$tfile: number of fq R2 to merge: ${#fqR2[@]}"

      echo "#BSUB -J PD1_fq_merge_bam_${tfile}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/PD1_fq_merge_bam_${tfile}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/PD1_fq_merge_bam_${tfile}.e 
#BSUB -cwd $scripts_dir
#BSUB -q medium
#BSUB -n 1
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N
zcat ${fqR1[@]} | gzip -c > $output_dir/${tfile}_R1.fastq.gz
zcat ${fqR2[@]} | gzip -c > $output_dir/${tfile}_R2.fastq.gz
" > ${scripts_dir}/${tfile}_fqMerge.lsf
      bsub < ${scripts_dir}/${tfile}_fqMerge.lsf
      sleep 2
  done

  ##consider to add fxn to generate the fqr1 fqr2 table...(for downstream use)

fi
