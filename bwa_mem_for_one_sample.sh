####################################################
# bwa mem for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

if [ $# != 4 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
    fq1=$1
    fq2=$2
    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R1*gz/}
    rg=$3
    rg=`echo $rg | sed -e 's/\./.t/g'|tr "." '\'`
    configFile=$4

    nt=$(cat $configFile | grep -w 'BWAMEM_NTHREAD' | cut -d '=' -f2)
    reference=$(cat $configFile | grep -w 'BWAMEM_REFERENCE' | cut -d '=' -f2)
    output=$(cat $configFile | grep -w 'BWAMEM_OUTPUT_DIR' | cut -d '=' -f2)
    echo $output
    echo -e $rg

    module load bwa/0.7.15
    module load samtools/1.3.1

    bwa mem \
        -t $nt \
        -T 0 \
        -R "${rg}" \
        $reference \
        $fq1 \
        $fq2 | \
        samtools view \
                 -Shb \
    -o $output/$samplename.bwamem.bam -

    ##sort
    module load picard/2.9.0
    java -jar /risapps/noarch/picard/2.9.0/build/libs/picard.jar SortSam \
         CREATE_INDEX=true \
         INPUT=$output/$samplename.bwamem.bam \
         OUTPUT=$output/$samplename.bwamem.sorted.bam \
         SORT_ORDER=coordinate \
         VALIDATION_STRINGENCY=STRICT
    rm $output/$samplename.bwamem.bam
fi
