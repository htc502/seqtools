####################################################
# bamfile index using samtools for one sample
# inputs: 
#    bamfile
#    configFile
####################################################

#!/bin/sh
if [ $# != 2 ]; then
    echo " usage: bamFile configFile";
else
    bamFile=$1
    configFile=$2
    bamDir=$(dirname "$bamFile")
    SampleName=$(basename "$bamFile")
    cd $bamDir
    samtools index $SampleName
fi
