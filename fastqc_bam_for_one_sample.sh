####################################################
# rsem+star pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: sampleName configFile";
else
    sample=$1
    configFile=$2

	################fastQC###############################
	fastqc_path=$(cat $configFile | grep -w 'FASTQC_PATH' | cut -d '=' -f2)
##  input_dir=$(cat $configFile | grep -w 'BAM_INPUT_PATH' | cut -d '=' -f2)
  output_dir=$(cat $configFile | grep -w 'FASTQC_OUTPUT_PATH' | cut -d '=' -f2)
  nthreads=$(cat $configFile | grep -w 'FASTQC_NTHREADS' | cut -d '=' -f2)

samplename=`basename $sample`
samplename=${samplename/.bam/}
##  bamfile=${sample}.bam
	
  output_dir1=${output_dir}/${samplename}

	mkdir -p $output_dir1
	cd $output_dir1

	echo "run fastQC on data before adaptor clipping..."
	echo "$fastqc_path/fastqc -t $nthreads -o $output_dir1 $sample 2>&1 |tee $output_dir1/${samplename}.fastqc.log"
	$fastqc_path/fastqc -t $nthreads -o $output_dir1 $sample 2>&1 |tee $output_dir1/${samplename}.fastqc.log

fi
