####################################################
# merge bam and markduplicates for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################
if [ $# != 4 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
java -jar picard.jar MergeSamFiles \
     ASSUME_SORTED=false \
     CREATE_INDEX=true \                 
[INPUT= <input.bam>]  \
    MERGE_SEQUENCE_DICTIONARIES=false \
    OUTPUT= <output_path> \
    SORT_ORDER=coordinate \
    USE_THREADING=true \
    VALIDATION_STRINGENCY=STRICT
fi
