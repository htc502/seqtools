####################################################
# fastqc for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: samplefilepath configFile";
else
    samplefilepath=$1
    configFile=$2

    ################fastQC###############################
    output_dir=$(cat $configFile | grep -w 'FASTQC_OUTPUT_PATH' | cut -d '=' -f2)

    fastq1=${samplefilepath}
    samplename=`basename $fastq1`
    samplename=${samplename/.fq.gz/}
    samplename=${samplename/.fq/}
    samplename=${samplename/.fastq.gz/}
    samplename=${samplename/.fastq/}
    
    output_dir1=${output_dir}/${samplename}

    mkdir -p $output_dir1
    cd $output_dir1

    echo "fastqc -o $output_dir1 $fastq1 &> $output_dir1/${samplename}.fastqc.log"
    fastqc -o $output_dir1 $fastq1 &> $output_dir1/${samplename}.fastqc.log

fi
