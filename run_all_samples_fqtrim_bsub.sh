#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create fastq_cutLowQual_sample_scripts subdir..."
    fastq_cutLowQual_sample_scripts_path=${SCRIPTS_PATH}/fastq_cutLowQual_sample_scripts
    mkdir -p $fastq_cutLowQual_sample_scripts_path

    ##cutlowqual will only do QC on those samples which undergo cutadapt...
    SAMPLE_LIST=$(cat $configFile | grep -w 'LOWQUAL_SAMPLE_LIST_FILE' | cut -d '=' -f2)
    cat ${SAMPLE_LIST} | while read line; do
	fields=( $line )
	samplename=${fields[0]}
	r1=${fields[1]}
	r2=${fields[2]}
	sampleoutputdir=${fields[3]}
	echo "#BSUB -J fqtrim_${samplename}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/fqtrim_${samplename}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/fqtrim_${samplename}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 131072
#BSUB -R rusage[mem=131072]
#BSUB -B
#BSUB -N

echo \"submitting fqtrim_${samplename} jobs\"
module load perl/5.22.1
perl ${SCRIPTS_PATH}/fastq_trim_for_one_sample.pl  $samplename $r1 $r2 $sampleoutputdir" >  ${fastq_cutLowQual_sample_scripts_path}/cutlowqual_${samplename}.lsf
	bsub< ${fastq_cutLowQual_sample_scripts_path}/cutlowqual_${samplename}.lsf
	sleep 1

    done
fi
