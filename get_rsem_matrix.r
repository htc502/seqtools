##remember to module load R/3.3.3-shlib on shark...
if(!require(dplyr)) stop('error loading dplyr')
if(!require(readr)) stop('erro loading readr')
if(!require(ggplot2)) stop('error loading ggplot2')
if(!require(rtracklayer)) stop('error loading rtracklayer')


args = commandArgs(trailingOnly = T)
if(length(args)!= 1) stop('usage: Rscript get_rsem_matrix.r configFile')
configFile=args[1]
## set before running
rsem_root_dir = system(command = paste0("cat ",configFile," | grep -w 'RSEMSTAR_OUTPUT_PATH' | cut -d '=' -f2"),intern = T)
output_dir=rsem_root_dir
gffFile = system(command = paste0("cat ",configFile," | grep -w 'RSEMSTAR_GFF_FILE' | cut -d '=' -f2"),intern = T)
##make tx2gene

gff = readGFF(gffFile,
              tags = c('gene_id','gene_type','gene_name','transcript_id'))
gene_gff = filter(gff, type == 'gene')
##sample id that wiil be used
##dir name to sample id
files = list.files(rsem_root_dir,recursive=F,full.names = T,pattern='.*.genes.results')
sampleNames=gsub('.genes.results','',basename(files))

tx_tpm = read_tsv(files[1]) %>% select(gene_id, TPM)
for(file in files[-1]) {
	print(paste0('merging file: ',file,'...\n'))
	tmp_tx = read_tsv(file) %>% select(gene_id, TPM)
	tx_tpm = left_join(tx_tpm,tmp_tx, by=c('gene_id'='gene_id')) 	
}

colnames(tx_tpm) = c('gene_id',sampleNames)
gene_annotation = gene_gff[match(tx_tpm$gene_id, gene_gff$gene_id),]

countMtr = tbl_df(data.frame(
SYMBOL=gene_annotation$gene_name,
ID=gene_annotation$gene_id,
Type=gene_annotation$gene_type,
select(tx_tpm,-gene_id),check.names=F))

pcg_countMtr = filter(countMtr, Type == 'protein_coding')
nonpcg_countMtr = filter(countMtr, Type != 'protein_coding')

write_tsv(pcg_countMtr, path=file.path(output_dir,'protein_coding_gene_countMatrix.tsv'))
write_tsv(nonpcg_countMtr, path=file.path(output_dir,'Non_protein_coding_gene_countMatrix.tsv'))


##want a correlation heatmap plot?
gene.df = select(pcg_countMtr, SYMBOL,ID)
dat.df = select(pcg_countMtr, -SYMBOL, -ID,-Type) 
##remove zero
low.idx = rowSums(dat.df) == 0
dat.df = dat.df[!low.idx,]
gene.df = gene.df[!low.idx,]
corrSamples = cor(dat.df,method = 'pearson')
library(pheatmap)
library(RColorBrewer)
pheatmap(mat=corrSamples,
         ##          color = colorRampPalette((brewer.pal(n = 7, name = "RdYlBu")))(100),
         ##         cluster_cols=F,
         ##         show_colnames = F,
         border_color = 'white',
         show_colnames=F,
         filename=file.path(output_dir,'sampleCorrelation.heatmap.pdf'))

