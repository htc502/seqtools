* Bioinformatics tools for HPC 

** flag.c  
   a sam FLAG field parser based on the sam specification: http://samtools.github.io/hts-specs/SAMv1.pdf  
** baseFreq.R  
   can be used to calculate a logo plot which indicate the nucleotide frequency of a sequence, it is slow but useful for barcode detection  
** getRefseqTranscript.R  
   the fxn in it takes a vector of entrez geneid and output a fasta file of transcripts corresponding to those genes  
** RNA-seq scripts used for NCBI sra data analysis
   generate an SrrRunTable.txt from sra run selector page
   pfetch.py -> wit SrrRunTable.txt, download sra data from NCBI sra repo;
   dumpAndMerge.py -> dump fastq from sra and merge fastq files according to the run table
   doTophat.py -> map reads using tophat
   seqCounts.py -> count read with htseq-count
** star2pass
   Configuration for two pass mapping using STAR.
*** a. fqList fq files suffix should be *R1.fastq.gz or *R1.fq.gz or *R1_fq.gz or *R1_fastq.gz. Any characters between R1 and gz will be removed to get the sample name, sample name will be used to generate resulting bamfiles
*** b. parameters needed:
**** run_all_samples:
     configFile: the config file path
     SCRIPTS_PATH: the root dir (where seqtools dir located) 
     STAR2PASS_SAMPLE_LIST_FILE: pair fastq file list(tab deliminated, no header)
**** for_one_sample:
     configFile
     STAR2PASS_GENOME_DIR: file path where star2pass use to generate tmp files
     STAR2PASS_GENOME_REF_FILE: genome reference file
     STAR2PASS_NTHREAD: star nthread
     STAR2PASS_SJDBOVERHANG: sjdboverhang param
     STAR2PASS_ROUND1_INDEX: index directory used to run round1
     STAR2PASS_ROUND1_DIR: round1 output directory
     STAR2PASS_ROUND2_DIR: round2 output directory
*** c. njob in =run_all_samples_star2pass= controls how many jobs running on the cluster, default is 3 (memory: 184320M, increase memory if njob is increased(consider mem=njob*60GB) )
*** steps of star2pass 
    1) generate novel splicing junction file. 2) regenerate intermediate genome reference.
    3) 2pass alignment. In terms of disk usage, the first step consumes little space, ~7.2M for one 
    sample. The second step is diskspace consuming. Dozens GB per sample. the third step file output
    size is depend on the sequencing depth.
    *** star2pass_sample_list_file should not include header

** Salmon
   Salmon is the next generation RNAseq quantification tool.
*** a.parameters needed:
**** run_all_samples:
     SCRIPTS_PATH
     SALMON_SAMPLE_LIST_FILE, it looks like:
     | s1R1.fq.gz | s1R2/fq.gz |
     | s2R1.fq.gz | s2R2/fq.gz |
     | s3R1.fq.gz | s3R2/fq.gz |
     ...
     no header included

**** for_one_sample:
     SALMON_BIN
     SALMON_INDEX
     SALMON_NP
     SALMON_OUTPUT_DIR
**** To invoke the whole thing 
     =sh run_all_samples_salmon_bsub.sh configfile=

** htseq-count
*** a.parameters needed:
**** run_all_samples:
     SCRIPTS_PATH
     HTSEQCOUNT_BAM_LIST: htseq-count will read bamfiles from this file
**** for_one_sample:
     HTSEQCOUNT_OUTPUT_PATH
     HTSEQCOUNT_GFF_FILE
     HTSEQCOUNT_STRAND (remember to check strand for each project before moving on!!! possible values: no/yes/reverse)
     HTSEQCOUNT_MODE (gdc default is intersection-nonempty)
     HTSEQCOUNT_ORDER (pos usually; htseqcount default is name, remember to set before moving on!!!)
     HTSEQCOUNT_MAX_READS_IN_BUFFER (to allow htseqcount deal with coordinate sorted bam files, increase it if memory overflow occured (high coverage datasets)). 
***** About strand specificty
      RNASEQC reports the proportion of sense (coding strand) derived reads and antisense derived reads. 
      if most of the end1 reads derived from sense and end2 reads derived from antisense:
      stranded. reason:
      sequencer always sequence reads from 5' to 3'(DNA polymerase can only extend DNA sequence in 5' -> 3' direction).
      For PE sequencing, End1 contains the same squences of 5' part of what you sequenced. 
      End2 contains the reverse compelement of the right most part of what you sequenced:

      End1 ------>
      5' ------------------> 3' (sequence input to sequencer)
      3' <------------------ 5'
                    <------ End2

      If most of end1 reads derived from sense, we infer that the sequence input is identical to sense(the coding strand).
***** htseq-count require aligner to assign NH optional field in sam file (to identify multiple mappers). 
      --nonunique option default to 'none'. which excludes multiple mappers.

*** b. how to run: prepare the HTSEQCOUNT_BAM_LIST file (no header), set parameters and go...
    file format for this bam list is:
    | /full/path/to/sample1.bam |
    | ...                       |
    | /full/path/to/sampleN.bam |
    no header included, can use the same list file for bamindex

*** c. use get_htseqcount_matrix.r to get count matrix(output protein coding gene matrix and other  genes' matrix separately)
    =Rscript get_htseqcount_matrix.r configfile=
*** d. use DESeq2 to do differential gene expression analysis
    set DESEQ2_SAMPLE_INFO_PATH
    Rscript DESeq2.r configfile or sh DESeq2.r.wrapper.sh (now we do lncRNA and pseudo genes with sh DESeq2.lncRNA.wrapper.sh)
    The sample info file looks like:
    | Sample_ID | condition |
    | ID1       | baseline  |
    | ID2       | baseline  |
    | ID3       | treatment |
    | ID4       | treatment |
    ...
    Header (Sample_ID and condition) is fixed. delimiter is tab. The code don't relevel condition which means we will take condition with lower alphabet order as the reference.
    The output filename of DESeq2 is based on sample info file.

** [vdjer]
*** a. how to run vdjer_quant: prepare a list file containing all vdjer mapping result __DIRs__
** [rnaseqc]
*** a. how to run (!!!Index Bam files before run RNASeQC!!!):
**** set variables(RNASEQC_JAR, BWA_PATH, RRNA_REF_FILE, GTF_FILE, RNASEQC_REF_GENOME,RNASEQC_INPUT_TABLE, RNASEQC_OUTPUT_PATH)
**** prepare RNASEQC_INPUT_TABLE, the table format is (note column can't be empty...,emacs may use space instead of tab, pay attention..):
     | SampleID | Bam          | Notes    |
     | ID1      | /path/to/bam | Note_ID1 |
     | ...      |              |          |
     | IDn      | /path/to/bam | Note_IDn |
       
     Notes columns shouldn't be empty. 
**** run =rna_seqc_bsub.sh=
**** log file name: RNAseqc.e and RNAseqc.o

** [picar bam merge]
*** a. how to run:
**** set up variables(PICARD_BAM_MERGE_SCRIPT_DIR, PICARD_BAM_MERGE_INPUT_TABLE, PICARD_BAM_MERGE_OUTPUT_PATH)
**** prepare PICARD_BAM_MERGE_INPUT_TABLE:
     | mergeName | dataName             |
     | mID1      | /path/to/bam         |
     | mID1      | /path/to/another/bam |
     | mID2      | /path/to/bam         |
     | mID2      | /path/to/another/bam |
     ...
** [fastq merge]
*** a. why? because tools like rsem/salmon map fq reads against transcriptome. to check library quality, we use star to map reads against genome, after confirming the quality, we merge multiple bam files belonging to one sample and do htseqcount. If you want to get rpkm/tpm quanlity, usually the tools start with mapping against transcriptome and give us the quantification results(rpkm/tpm.txt), you have no chance to merge the individual library wise bam files. that's why we do fq merge before using these kind of tools.
*** b. how to run:
**** set up variables: FQ_MERGE_SCRIPT_DIR; FQ_MERGE_INPUT_TABLE; FQ_MERGE_OUTPUT_PATH
**** prepare the input table like this:
     | ID  | R1                   | R2                   |
     | ID1 | /path/to/R1_l1.fq.gz | /path/to/R2_l1.fq.gz |
     | ID1 | /path/to/R1_l2.fq.gz | /path/to/R2_l2.fq.gz |
     | ID1 | /path/to/R1_l3.fq.gz | /path/to/R2_l3.fq.gz |
     | ID1 | /path/to/R1_l4.fq.gz | /path/to/R2_l4.fq.gz |
     | ID2 | /path/to/R1_l1.fq.gz | /path/to/R2_l1.fq.gz |
     ....
     the header name is not fixed, but shouldn't be ignored. Merged files with haev name
     like ID1_R1.fastq.gz; ID1_R2.fastq.gz etc...

     =sh fq_merge_bsub.sh configfile=
       
** [polysolver (HLA genotyping)]
*** a. how to run:
**** setup variables: POLYSOLVER_OUTPUT_PATH; POLYSOLVER_NTHREADS; POLYSOLVER_SAMPLE_LIST_FILE (bam file list)
**** prepare the input table like:
     | /path/to/bamfiles1 |
     | /path/to/bamfiles2 |
     ...
     don't include header in this file, to start the job: sh run_all_samples_polysolver.sh config
** [fastqc-bam]
*** a. how to run:
**** varaibles: FASTQC_OUTPUT_PATH; FASTQC_NTHREADS; BAM_LIST
**** prepare the input table:
     | /path/to/bamfiles1 |
     | /path/to/bamfiles2 |
     | /path/to/bamfiles3 |
     ...
     no header in the table file..

** [fastqc-raw]
*** a. how to run:
**** varaibles: FASTQC_OUTPUT_PATH; FASTQ_LIST_FILE
**** prepare the input table:
     | /path/to/fastqfiles1 |
     | /path/to/fastqfiles2 |
     | /path/to/fastqfiles3 |
     ...
     no header in the table file..

** [featurecounts]
*** a.parameters needed:
**** run_all_samples:
     SCRIPTS_PATH
     FEATURECOUNTS_BAM_LIST: featurecounts will read bamfiles from this file
**** for_one_sample:
     FEATURECOUNTS_OUTPUT_PATH
     FEATURECOUNTS_GFF_FILE
     FEATURECOUNTS_STRAND (remember to check strand for each project before moving on!!!
     0:unstranded, 1:stranded, 2:reversely stranded)
     FEATURECOUNTS_NTHREADS 

*** b. how to run: prepare the FEATURECOUNTS_BAM_LIST file (no header), set parameters and go...
*** c. use get_featurecounts_matrix.r to get count matrix(output protein coding gene matrix and other  genes' matrix separately)
    =Rscript get_featurecounts_matrix.r configfile=
    Will generate a matrix containing all available samples inside featurecount output dir.
*** d. use DESeq2 to do differential gene expression analysis
    set DESEQ2_SAMPLE_INFO_PATH
    =Rscript DESeq2.r configfile featurecounts (for wrapper sh see htseqcount section)=
    The sample info file looks like:
    | Sample_ID | condition |
    | ID1       | baseline  |
    | ID2       | baseline  |
    | ID3       | treatment |
    | ID4       | treatment |
    ...
    Header (Sample_ID and condition) is fixed. delimiter is tab. The code don't relevel condition which means we will take condition with lower alphabet order as the reference. featurecounts is REQUIRED. otherwise the code will run into htseq-count directory.
*** Explanation of low Successfully assigned fragments rate
The number of fragments reported in featurecounts error.log is the number of total alignments.
It is better to use the total number of read pairs of the input fastq file to estimate the rate mentioned above.

** [fastq low quality trim (pair-end RNAseq)]
*** a. This tool drops bad reads if the read has the number of bad quality nucleotides(Q<20) > 15.
*** b. how to run: prepare a file table like the following:
    | sampleName1 | /path/to/sample1/R1 | /path/to/sample1/R2 | /path/to/output/of/the/sample1 |
    | sampleName2 | /path/to/sample2/R1 | /path/to/sample2/R2 | /path/to/output/of/the/sample2 |
    | sampleName3 | /path/to/sample3/R1 | /path/to/sample3/R2 | /path/to/output/of/the/sample3 |
    | sampleName4 | /path/to/sample4/R1 | /path/to/sample4/R2 | /path/to/output/of/the/sample4 |
    | sampleName5 | /path/to/sample5/R1 | /path/to/sample5/R2 | /path/to/output/of/the/sample5 |
*** c. set parameter: LOWQUAL_SAMPLE_LIST_FILE to the file path of the table
** [MIXCR T and B cell repotoire analysis
*** a. parameters needed: MICXR_BIN; MIXCR_OUTPUT_DIR; MIXCR_SAMPLE_LIST_FILE
*** b. how to run:
**** set up the variables mentioned above
**** prepare the file list table( MIXCR_SAMPLE_LIST_FILE):
     | /full/path/to/fq1 | /full/path/to/fq2 |
     | ...               |                   |
     | /full/path/to/fq1 | /full/path/to/fq2 |
     no header included.
**** invoke run_all_samples_mixcr_bsub.sh configfile

** [bamIndex]
*** a. index bam using samtools index. parameters needed: BAMINDEX_BAM_LIST
**** b. how to run: prepare a file table:
     | /full/path/to/sample1.bam |
     | ...                       |
     | /full/path/to/sampleN.bam |
     no header included
     and invoke run_all_samples_bamIndex_bsub.sh configfile.
     bamindex will first `cd` to bam file dir and then invoke samtools index `basename bamFile`.
** [DNAseq pipeline]
*** bwa mem align
**** a. parameters needed: BWAMEM_NTHREAD; BWAMEM_REFERENCE; BWAMEM_OUTPUT_DIR; BWAMEM_SAMPLE_LIST_FILE
**** b. prepare sample list file:
     | /full/path/to/sample1.fq |
     | ...                      |
     | /full/path/to/sampleN.fq |
     no header included
*** picard merge for Read group and index
**** a. parameters needed: PICARD_BAM_MERGE_TABLE, the table looks like:
     | /full/path/merged1.bam | /full/path/part1.bam... | /full/path/partn.bam |
     | ...                    |                         |                      |
     | /full/path/mergedn.bam | /full/path/part1.bam... | /full/path/partn.bam |
	  
** [cutadapt-config]
*** parameters:CUTADAPT_PARAMS for adapter specifying; CUTADAPT_OUTPUT_PATH; CUTADAPT_FASTQ_PAIR_LIST_FILE
*** parameters for single end data:FASTQ_LIST_FILE
**** lines in CUTADAPT_FASTQ_PAIR_LIST_FILE has the form: fq1\tfq2
** [gatk4ascat-config]
*** parameters: GATK4ASCAT_GENOME_REF_FILE; GATK4ASCAT_CAPTURE_BED_FILE; GATK4ASCAT_OUTPUT_DIR; GATK4ASCAT_JAR; GATK4ASCAT_BAM_PAIR_LIST_FILE
*** bam pair list without header, bam pair table format:
    | ID | /path/to/tumorBam | /path/to/normalBam (ID usually takes tumor sample Name) |
    ....
    a return seems to be nacessary for the last line to be taken into consiteration 
    To run the tool, invoke =sh run_all_samples_gatk4ascat.sh configFile=
** [gatk-reformat-config]
*** parameters: GATKVCF_REFORM_TABLE_FILE; 
    table format: first column is the name of tumor sample ,second column is the file path of vcf file; third column is the filepath of resulting snp.txt
    | tumorName | /path/to/unif.snp.vcf | /path/to/result.unif.snp.txt |
    | ...       |                       |                              |
    | tumorName | /path/to/unif.snp.vcf | /path/to/result.unif.snp.txt |
*** run: =sh gatk4ascat_reformator.sh configFile=
    
** [cnvkit4ascat-config]
*** parameters:GATK4ASCAT_CAPTURE_BED_FILE; CNVKIT4ASCAT_OUTPUT_DIR; GATK4ASCAT_BAM_PAIR_LIST_FILE
*** run: =sh run_all_samples_cnvkit4ascat.sh configFile=
    
** [runascat-config]
*** parameters:GATKVCF_REFORM_TABLE_FILE; CNVKIT4ASCAT_OUTPUT_DIR; ASCAT_DIR; 
*** run: sh run_ascat.r.warpper.sh will submit the lsf job.
    
    ascat log file: =~/log/run_ascat.e/o=
** [sequenza-config]
*** parameters: SEQUENZA_GENOME_REF_FILE; SEQUENZA_CAPTURE_BED_FILE; SEQUENZA_OUTPUT_DIR; SEQUENZA_BAM_PAIR_LIST_FILE; GC50; SEQUENZA_PLOTS_DIR
*** SEQUENZA_BAM_PAIR_LIST_FILE is in the same format as GATK4ASCAT_BAM_PAIR_LIST_FILE
    !!!add a return at the end of the last line in the table.!!!
    to run sequenza: =sh run_all_samples_sequenza.sh configFile=
    SEQUENZA_PLOTS_DIR is used by =sequenza_downstream.r= for downstream plots generation.
** [sciclone-config]
*** parameters: GATKVCF_REFORM_TABLE_FILE; SEQUENZA_PLOTS_DIR; SCICLONE_CNV_INPUT; GATKVCF_REFORM_TABLE_FILE; SCICLONE_MUTATION_INPUT;SCICLONE_OUTPUT
    sciclone needs copy number and mutect results. The file name is identical to sequenza results.
** [todo]
   Add singleRNAseq routine tools: cell cycle estimation; gsva scoring; singleR and vGATE for cell type identification.
*** singleR (cell type identification)
*** vGATE (cell type identification)
** [gsva Score]
*** parameters: GSVA_Ncells;GSVA_NsegLen;GSVA_Ncore;GSVA_OUTPUTD;GSVA_GMTFILE;GSVA_seuratOBJ_FILE
    GSVA_seuratOBJ_FILE is seurat object file saved using saveRDS
    GSVA_Ncells: total number of cells in the data
    GSVA_NsegLen: # of cells for each child job
    GSVA_Ncore: # of cores used for ssgsva
*** to run: ssh gsva-v1-bsub.sh configFile
*** todo: let bsub.sh know how many cells in the data and we can remove GSVA_Ncells param.
*** cell cycle scoring

