####################################################
#  ascat: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)

    run_ascat_sample_scripts_path=${SCRIPTS_PATH}/run_ascat_sample_scripts
    mkdir -p $run_ascat_sample_scripts_path

    echo "#BSUB -J run_ascat
#BSUB -W 1:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/run_ascat.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/run_ascat.e 
#BSUB -q short
#BSUB -n 1
#BSUB -M 65536
#BSUB -R rusage[mem=65536]
#BSUB -B
#BSUB -N

module load R/3.3.3-shlib
module load bedtools/2.24.0
Rscript $SCRIPTS_PATH/run_ascat.r $configFile
" >  ${run_ascat_sample_scripts_path}/run_ascat.lsf
    bsub <  ${run_ascat_sample_scripts_path}/run_ascat.lsf 
    sleep 1

fi
