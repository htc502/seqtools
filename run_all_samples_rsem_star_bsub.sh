####################################################
# rsem star pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create rsemstar_mapping_sample_scripts subdir..."
    rsemstar_mapping_sample_scripts_path=${SCRIPTS_PATH}/rsemstar_mapping_sample_scripts
    mkdir -p $rsemstar_mapping_sample_scripts_path

    ##rsemstar will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'RSEMSTAR_SAMPLE_LIST_FILE' | cut -d '=' -f2)
    np=$(cat $configFile | grep -w 'RSEMSTAR_NTHREAD' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        fields=( $line )
        fq1=${fields[0]}
        fq2=${fields[1]}
        samplename=`basename $fq1`
        samplename=${samplename/[.|_]R1*gz/}
        echo "#BSUB -J ${samplename}_rsemstar
#BSUB -W 48:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${samplename}_rsemstar.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${samplename}_rsemstar.e 
#BSUB -q long
#BSUB -n ${np}
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

sh ${SCRIPTS_PATH}/rsem_star_for_one_sample.sh $fq1 $fq2 ${SCRIPTS_PATH}/config/tool_info.txt" >  ${rsemstar_mapping_sample_scripts_path}/rsemstar_${samplename}.lsf
         bsub <  ${rsemstar_mapping_sample_scripts_path}/rsemstar_${samplename}.lsf 
        sleep 2
    done

fi
