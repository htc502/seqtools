
## this function receive a vector of entrez gene ids and generate a fasta file containing gene related transcript sequences
## rm.predict can be set to remove transcripts which has 'PREDICTED' word in its title
## toggle verbose to check more detailed information that the fxn will generate
getRefSeqTranscripts <- function(geneIDs, f.out,rm.predict = T, verbose= F) {
    if(!require(rentrez))
        stop("error loading rentrez package\n")
    if(!require(seqinr))
        stop('error loading seqinr package\n')
    seq.list <- list()
    for(id in geneIDs) {
        if(is.na(id))
            next
        print(paste0('[entrez_link]: retrieving rna transcript IDs for gene \'',id,'\''))
        ezlink <- entrez_link(dbfrom = 'gene', id = id, db = 'nuccore')
        rnaIDs <- ezlink$gene_nuccore_refseqrna
        print(paste0('[entrez_link]: got ',length(rnaIDs),' rna ids, try to fetch sequence(s)'))
        for(rnai in rnaIDs) {
            if(verbose)
                print(paste0('[entrez_fetch]: retrieving fasta sequence informention for rnaID: \'',id,'\''))
            fasta <- entrez_fetch(db = 'nuccore', id = rnai,
                                  rettype = 'fasta')[1]
            if(is.null(fasta))
                warning(paste0('error get ',id,' gene\'s transcript(transcriptID: ',rnai,
                        ' )\n'));
            tmp <- strsplit(fasta, split = '\n')[[1]]
            fa.name <- gsub(' ','_',gsub('>','',tmp[1]))
            fa.seq <- paste(tmp[-1],collapse='')
            fa.obj <- as.SeqFastadna(fa.seq, name = fa.name,
                                     Annot = fa.name)
            if(verbose) {
                print(paste0('[transcriptInfo sequenceName]',getName(fa.obj)))
                print(paste0('[transcriptInfo sequenceAnnot]',getAnnot(fa.obj)))
            }
            seq.list[[length(seq.list)+1]] <- fa.obj
            rm(fa.name);rm(fa.seq);rm(fa.obj);rm(tmp)
        }
    }
    if(rm.predict) {
        pred.ind <- grepl('PREDICTED', unlist(lapply(seq.list, function(x) attributes(x)$Annot)))
        if(sum(!pred.ind) != 0) {
            print(paste0(sum(pred.ind),' sequences are thought to be PREDICTED transcript, ignoring them...'))
            seq.list <- seq.list[!pred.ind]
        }
    }
    write.fasta(seq.list, names = unlist(lapply(seq.list,getName)),file.out = f.out,nbchar = max(unlist(lapply(seq.list, length))))
}
