# by shaojun zhang, SZhang19@mdanderson.org
#!/usr/bin/env python
import os,time,sys

#outfileName='/rsrch2/genomic_med/szhang19/Michael/Rashmi/DNAseq/mutectSNPprocess/2-S-17-022031'
#infileName='/rsrch2/genomic_med/szhang19/Michael/Rashmi/DNAseq/mutectSNP/2-S-17-022031'
#sampleName='2-S-17-022031'
infileName = sys.argv[1]
outfileName = sys.argv[2]
sampleName = sys.argv[3]

outfile=open(outfileName,'w')
outfile.write('CHROM\tPOS\tID\tREF\tALT\tQUAL\tMQ\tNormal.GT\tNormal.REF.DP\tNormal.ALT.DP\tTumor.GT\tTumor.REF.DP\tTumor.ALT.DP\n')
with open(infileName) as infile:
    for _ in xrange(117):   ## skip multiple header lines of VCF
        header=next(infile)
        header1=header.split("\t")
        header1[-1] = header1[-1].strip()
    index_tumor=header1.index(sampleName)
    if index_tumor==9:
        index_normal=10
    else:
        index_normal=9
    i=1
    for line in infile:
        i=i+1
        info=line.strip().split('\t')
        chr=info[0]
        pos=info[1]
        ID=info[2]
        REF=info[3]
        ALT=info[4]
        QUAL=info[5]
        filter=info[6]
        inform=info[7]
        MQ=inform.split(';')[11]
        MQ=MQ.split('=')[1]
        normal=info[index_normal]  ##
        normal0=normal.split(':')[0]
        tumor=info[index_tumor]    ##
        tumor0=tumor.split(':')[0]
        parsing = False
        if filter=="." and MQ >=30:
            parsing = True
        if parsing:
            if normal0 == './.':
                continue
            normal_GT=normal.split(':')[0]
            normal_AD=normal.split(':')[1]
            normal_REF_DP=normal_AD.split(',')[0]
            normal_ALT_DP=normal_AD.split(',')[1]
            if tumor0 == './.':
                continue
            tumor_GT=tumor.split(':')[0]
            tumor_AD=tumor.split(':')[1]
            tumor_REF_DP=tumor_AD.split(',')[0]
            tumor_ALT_DP=tumor_AD.split(',')[1]
            result=(chr,pos,ID,REF,ALT,QUAL,MQ,normal_GT,normal_REF_DP,normal_ALT_DP,tumor_GT,tumor_REF_DP,tumor_ALT_DP)
            outfile.write('%s\n'%'\t'.join(result))
outfile.close()
infile.close()
