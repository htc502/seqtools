wd='/data/ghan/get_lncRNA_from_array/blast/data/rnaseqData/sra/data/star_mapping'
setwd(wd)
d=list.dirs('.',recursive=F,full.names=F)
dat <- c()
for( di in d) {
	statd <- paste0(di,'_rsem_including_star.stat')
	dati <- readLines(paste0(di,'/',statd,'/',di,'_rsem_including_star.cnt'),n=1)
	dati <- strsplit(dati,split=' ')[[1]]
	print(dati)
	dat <- rbind(dat,dati)
}
rownames(dat) <- d
colnames(dat) <- c('unaligned',
		'aligned',
		'failed(too many align)',
		'total')
write.csv(dat,file='/data/ghan/get_lncRNA_from_array/blast/data/rnaseqData/sra/star_mapping_scripts/rsem_mapping_stat.csv')
	
