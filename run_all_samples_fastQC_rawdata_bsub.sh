####################################################
# fastQC for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create fastQC_rawdata_sample_scripts subdir..."
    fastQC_sample_scripts_path=${SCRIPTS_PATH}/fastQC_rawdata_sample_scripts
    mkdir -p ${fastQC_sample_scripts_path}
    FASTQ_LIST_FILE=$(cat $configFile | grep -w 'FASTQ_LIST_FILE' | cut -d '=' -f2)
    njob=11
    cat $FASTQ_LIST_FILE | while read line; do
        let c=c+1
        FASTQ=`basename ${line}`
        FASTQ=${FASTQ/.fq.gz/}
        FASTQ=${FASTQ/.fq/}
        FASTQ=${FASTQ/.fastq.gz/}
        FASTQ=${FASTQ/.fastq/}
        echo "#BSUB -J fqc_${FASTQ}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/fqc_${FASTQ}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/fqc_${FASTQ}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N

module load fastqc/0.11.5

sh ${SCRIPTS_PATH}/fastqc_rawdata_for_one_sample.sh ${line} ${SCRIPTS_PATH}/config/tool_info.txt " > ${fastQC_sample_scripts_path}/fastQC_rawdata_${FASTQ}.lsf
        bsub< ${fastQC_sample_scripts_path}/fastQC_rawdata_${FASTQ}.lsf
        sleep 2
    done
fi
