rm(list = ls())
args = commandArgs(trailingOnly = T)
if(!(length(args)== 1) ) stop('usage: Rscript run_ascat.r configFile')
configFile=args[1]
sampleInfo = system(command = paste0("cat ",configFile," | grep -w 'GATKVCF_REFORM_TABLE_FILE' | cut -d '=' -f2"),intern = T)
cnvkitDir = system(command = paste0("cat ",configFile," | grep -w 'CNVKIT4ASCAT_OUTPUT_DIR' | cut -d '=' -f2"),intern = T)
ascatDir = system(command = paste0("cat ",configFile," | grep -w 'ASCAT_DIR' | cut -d '=' -f2"),intern = T)
library(readr)
library(scales)
reformTable = read_tsv(sampleInfo,
                       col_names = F)
print('get baf.bed...')
for (i in 1:nrow(reformTable)){
  sample = reformTable[i,]$X3
  print(sample)
  unif.snp = read.delim(sample,header=TRUE, sep="\t",stringsAsFactors=FALSE)
  outputdir=dirname(sample)
  samplename=basename(sample);samplename = gsub('.unif.snp.txt','',samplename)
  unif.snp$BAF_normal = unif.snp$Normal.ALT.DP /(unif.snp$Normal.REF.DP + unif.snp$Normal.ALT.DP)
  unif.snp$BAF_tumor = unif.snp$Tumor.ALT.DP /(unif.snp$Tumor.REF.DP + unif.snp$Tumor.ALT.DP)
  unif.snp1 = unif.snp[,c("CHROM","POS","POS","REF","ALT","BAF_normal","BAF_tumor")]
  write.table(unif.snp1, file = file.path(ascatDir,paste0(samplename,".baf.bed")),
              row.names = FALSE, col.names=FALSE, sep="\t", quote=FALSE)

  CNVkit.normal = read.delim(file.path(cnvkitDir,paste(samplename,'-matchedNormal',sep="")),header=TRUE, sep="\t",stringsAsFactors=FALSE)
  CNVkit.normal1 = CNVkit.normal[,c("chromosome","start","end","log2")]
  write.table(CNVkit.normal1, file = file.path(ascatDir,paste0(samplename,"-T.CNVkit.normal.bed", sep="")),
              row.names = FALSE, col.names=FALSE, sep="\t", quote=FALSE)

  CNVkit.tumor = read.delim(file.path(cnvkitDir,samplename),header=TRUE, sep="\t",stringsAsFactors=FALSE)
  CNVkit.tumor1 = CNVkit.tumor[,c("chromosome","start","end","log2")]
  write.table(CNVkit.tumor1, file = file.path(ascatDir,paste0(samplename,"-T.CNVkit.tumor.bed", sep="")),
              row.names = FALSE, col.names=FALSE, sep="\t", quote=FALSE)

  ##bedtools intersect
  cmd1 = paste0('bedtools intersect -a ',file.path(ascatDir,paste0(samplename,".baf.bed")),
                ' -b ',file.path(ascatDir,paste0(samplename,"-T.CNVkit.normal.bed")),
                ' -wa -wb > ',file.path(ascatDir,paste0(samplename,'normal.GATKunitype.CNVkit.bed')))
  system(cmd1)
  cmd2 = paste0('bedtools intersect -a ',file.path(ascatDir,paste0(samplename,".baf.bed")),
                ' -b ',file.path(ascatDir,paste0(samplename,"-T.CNVkit.tumor.bed")),
                ' -wa -wb > ',file.path(ascatDir,paste0(samplename,'tumor.GATKunitype.CNVkit.bed')))
  system(cmd2)

  ##last prep step
  print('get logR table...')
  tumorbed = read.delim(file.path(ascatDir,paste0(samplename,'tumor.GATKunitype.CNVkit.bed')),header=FALSE, sep="\t",stringsAsFactors=FALSE)
  normal.BAF = tumorbed[,c(1,2,6)]
  colnames(normal.BAF) = c("chrs","pos","BAF")
  row.names(normal.BAF) = paste("SNP",c(1:nrow(normal.BAF)),sep="")
  tumor.BAF = tumorbed[,c(1,2,7)]
  colnames(tumor.BAF) = c("chrs","pos","BAF")
  row.names(tumor.BAF) = paste("SNP",c(1:nrow(tumor.BAF)),sep="")
  tumor.logR = tumorbed[,c(1,2,11)]
  colnames(tumor.logR) = c("chrs","pos","logR")
  row.names(tumor.logR) = paste("SNP",c(1:nrow(tumor.logR)),sep="")
  tumor.logR[,"logR"] = rescale(tumor.logR[,"logR"], to=c(-1,1))

  normalbed = read.delim(file.path(ascatDir,paste0(samplename,'normal.GATKunitype.CNVkit.bed')),header=FALSE, sep="\t",stringsAsFactors=FALSE)
  normal.logR = normalbed[,c(1,2,11)] #?
  colnames(normal.logR) = c("chrs","pos","logR")
  row.names(normal.logR) = paste("SNP",c(1:nrow(normal.logR)),sep="")
  normal.logR[,"logR"] = rescale(normal.logR[,"logR"], to=c(-1,1))

  write.table(normal.BAF, file = file.path(ascatDir,paste0(samplename,".normal.BAF.txt")),
              row.names = TRUE, col.names=TRUE, sep="\t", quote=FALSE)
  write.table(tumor.BAF, file = file.path(ascatDir,paste0(samplename,".tumor.BAF.txt")),
              row.names = TRUE, col.names=TRUE, sep="\t", quote=FALSE)
  write.table(normal.logR, file = file.path(ascatDir,paste0(samplename,".normal.logR.txt")),
              row.names = TRUE, col.names=TRUE, sep="\t", quote=FALSE)
  write.table(tumor.logR, file = file.path(ascatDir,paste0(samplename,".tumor.logR.txt")),
              row.names = TRUE, col.names=TRUE, sep="\t", quote=FALSE)
}

##ASCAT
print('run ASCAT')
library(ASCAT)
library(readr)
All.samples = reformTable$X3
All.samples = basename(All.samples)
for (i in 1:length(All.samples)){
  ## i = 244
  print(c(i,All.samples[i]))
  samplename=basename(All.samples[i]);samplename = gsub('.unif.snp.txt','',samplename)
  dir.create(file.path(ascatDir,samplename),recursive=T, showWarnings = FALSE)
  setwd(file.path(ascatDir,samplename))
  ascat.bc = ascat.loadData(file.path(ascatDir,paste0(samplename,".tumor.logR.txt")),
                            file.path(ascatDir,paste0(samplename,".tumor.BAF.txt")),
                            file.path(ascatDir,paste0(samplename,".normal.logR.txt")),
                            file.path(ascatDir,paste0(samplename,".normal.BAF.txt")))
  ascat.plotRawData(ascat.bc)
  ascat.bc = ascat.aspcf(ascat.bc)
  ascat.plotSegmentedData(ascat.bc)
  print('ploting...')
  ascat.output = ascat.runAscat(ascat.bc,
                                        #gamma = 1, 
                                        #rho_manual =0.25, 
                                        #psi_manual = 0.5, 
                                pdfPlot = T, 
                                y_limit = 5)
  save(ascat.output, file = paste(samplename,"RData",sep="."))
}


