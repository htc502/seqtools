####################################################
# bam2fastq for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/bash

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    echo "$c"
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create bam2fq_sample_scripts subdir in $SCRIPTS_PATH..."
    bam2fq_sample_scripts_path=$(cat $configFile | grep -w 'TMP_SCRIPT_PATH' | cut -d '=' -f2)
    mkdir -p ${bam2fq_sample_scripts_path}
    BAM_LIST=$(cat $configFile | grep -w 'BAM2FASTQ_BAM_LIST' | cut -d '=' -f2)
    echo "looping through each bam file..."
    cat $BAM_LIST | while read line; do
        echo "$line"
        samplename=`basename $line`
        samplename=${samplename/.bam/}
        let c=c+1
        while [ " " == " " ]
        do
            d=`ps -aux | grep 'bam2fastq' | wc -l`
            #echo "line=$line"
            if [ $d -le 10 ]
            then
                echo "$c=$line"
                echo "-------------------------------"
                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh
sh ${SCRIPTS_PATH}/bam2fastq.sh $line  ${SCRIPTS_PATH}/config/tool_info.txt " > ${bam2fq_sample_scripts_path}/bam2fq_${samplename}.sh
                chmod 755 ${bam2fq_sample_scripts_path}/bam2fq_${samplename}.sh
                sh ${bam2fq_sample_scripts_path}/bam2fq_${samplename}.sh &
                sleep 1
                break
            else
                sleep 30
            fi
        done
        echo "end"
    done
fi
