####################################################
# picard bam merge for pipeline
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1

	################fastQC###############################
  JAR_PICARD=$(cat $configFile | grep -w 'PICARD_JAR' | cut -d '=' -f2)

  scripts_dir=$(cat $configFile | grep -w 'PICARD_BAM_MERGE_SCRIPT_DIR' | cut -d '=' -f2)
  mkdir -p ${scripts_dir}

  input_table=$(cat $configFile | grep -w 'PICARD_BAM_MERGE_INPUT_TABLE' | cut -d '=' -f2)
  output_dir=$(cat $configFile | grep -w 'PICARD_BAM_MERGE_OUTPUT_PATH' | cut -d '=' -f2)

	mkdir -p $output_dir
	cd $output_dir

	echo "run picard merge..."
  targetBamfiles=( $(cut -f 1 $input_table | tail -n +2 |sort| uniq) )
  for tfile in ${targetBamfiles[@]}
  do
      bamFiles=( $(grep  -P "^$tfile\t" $input_table | cut -f2 | uniq) )
      echo "$tfile: number of bamfiles to merge: ${#bamFiles[@]}"
      inputParams=( "${bamFiles[@]/#/I=}" )
      echo "#BSUB -J PD1_picard_merge_bam_${tfile}
#BSUB -W 4:00
#BSUB -o /rsrch2/genomic_med/ghan1/log/PD1_picard_merge_bam_${tfile}.o 
#BSUB -e /rsrch2/genomic_med/ghan1/log/PD1_picard_merge_bam_${tfile}.e 
#BSUB -cwd /rsrch2/genomic_med/ghan1/PD1_inhibitor_miles_analysis/seqtools
#BSUB -q medium
#BSUB -n 2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org
module load jdk/1.8.0_45
module load picard/2.9.0
java -jar $JAR_PICARD MergeSamFiles ${inputParams[*]} O=$output_dir/${tfile}.bam ASSUME_SORTED=true SORT_ORDER=coordinate  2>&1 |tee $output_dir/picard_bam_merge_${tfile}.log
java -jar $JAR_PICARD BuildBamIndex I=$output_dir/${tfile}.bam O=${output_dir}/${tfile}.bai 2>&1 |tee $output_dir/picard_bam_index_${tfile}.log

##htseqcount style bai
ln -s ${output_dir}/${tfile}.bai ${output_dir}/${tfile}.bam.bai" > ${scripts_dir}/${tfile}.lsf
      bsub < ${scripts_dir}/${tfile}.lsf
      sleep 2
  done

fi
