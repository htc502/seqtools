####################################################
# rsem+star pipeline: fastQC for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    CUTADAPT_OUTPUT_PATH=$(cat $configFile | grep -w 'CUTADAPT_OUTPUT_PATH' | cut -d '=' -f2)
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create fastQC_after_cutadpt_sample_scripts subdir..."
    fastQC_after_cutadpt_sample_scripts_path=${SCRIPTS_PATH}/fastQC_rawdata_sample_scripts
    mkdir -p $fastQC_after_cutadpt_sample_scripts_path

    ##fastQC only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=`ls ${CUTADAPT_OUTPUT_PATH}/*.cutadapt.fq`

    echo ${FASTQ_LIST}| while read line; do
        echo "$line"
        let c=c+1
        while [ " " == " " ]
        do
            d=`ps -aux | grep 'fastqc' | wc -l`
            #echo "line=$line"
            FASTQ=${line/.cutadapt.fq//}
            if [ $d -le 6 ]
            then
                echo "$c=$line"
                echo "-------------------------------"
                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh ${SCRIPTS_PATH}/fastqc_after_cutadapt_for_one_sample.sh ${FASTQ} ${SCRIPTS_PATH}/config/tool_info.txt " >  ${fastQC_after_cutadpt_sample_scripts_path}/qc_after_cutadapt_${FASTQ}.sh
                chmod 755  $fastQC_after_cutadpt_sample_scripts_path/qc_after_cutadapt_${FASTQ}.sh
                sh  $fastQC_after_cutadpt_sample_scripts_path/qc_after_cutadapt_${FASTQ}.sh  &
                sleep 2
                break
            else
                sleep 30
            fi
        done
        echo "end"
    done

fi
