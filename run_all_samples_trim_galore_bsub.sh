####################################################
# trim_galore: trim_galore for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create trim_galore_sample_scripts subdir..."
    trim_galore_sample_scripts_path=${SCRIPTS_PATH}/trim_galore_rawdata_sample_scripts
    mkdir -p ${trim_galore_sample_scripts_path}
    FASTQ_LIST=$(cat $configFile | grep -w 'TRIM_GALORE_FASTQ_PAIR_LIST_FILE' | cut -d '=' -f2)
    cat $FASTQ_LIST | while read line; do
        echo "$line"
        fields=( $line )
        fq1=${fields[0]}
        fq2=${fields[1]}
            FASTQ=`basename ${fq1}`
            samplename=${FASTQ/[.|_]R[1|2]*gz/}
            echo "#BSUB -J trim_galore_${samplename}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/trim_galore_${samplename}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/trim_galore_${samplename}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N

sh ${SCRIPTS_PATH}/trim_galore_for_one_sample.sh $fq1 $fq2 ${SCRIPTS_PATH}/config/tool_info.txt " > ${trim_galore_sample_scripts_path}/trim_galore_rawdata_${samplename}.lsf
            bsub< ${trim_galore_sample_scripts_path}/trim_galore_rawdata_${samplename}.lsf
            sleep 2
    done

fi
