####################################################
# rna-seqc for pipeline
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1

	################fastQC###############################
    script_dir0=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
  script_dir=${script_dir0}/rnaseqc_sample_script
  mkdir -p ${script_dir}
	rnaseqc_jar=$(cat $configFile | grep -w 'RNASEQC_JAR' | cut -d '=' -f2)
bwa_path=$(cat $configFile | grep -w 'BWA_PATH' | cut -d '=' -f2)
rrna_file=$(cat $configFile | grep -w 'RRNA_REF_FILE' | cut -d '=' -f2)
gtf_file=$(cat $configFile | grep -w 'GTF_FILE' | cut -d '=' -f2)
reference_file=$(cat $configFile | grep -w 'RNASEQC_REF_GENOME' | cut -d '=' -f2)

  input_table=$(cat $configFile | grep -w 'RNASEQC_INPUT_TABLE' | cut -d '=' -f2)
  output_dir=$(cat $configFile | grep -w 'RNASEQC_OUTPUT_PATH' | cut -d '=' -f2)

	mkdir -p $output_dir
	cd $output_dir

  echo "#BSUB -J RNAseqc
#BSUB -W 240:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/RNAseqc.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/RNAseqc.e 
#BSUB -q long
#BSUB -n 2
#BSUB -M 163840
#BSUB -R rusage[mem=163840]
#BSUB -B
#BSUB -N

echo "submitting RNAseqc jobs"
module load bwa/0.7.12
module load jdk/1.7.0_79 

java -jar $rnaseqc_jar -r ${reference_file} -bwa ${bwa_path} -BWArRNA ${rrna_file}  -o $output_dir -s ${input_table} -t $gtf_file 2>&1 |tee $output_dir/rnaseqc.log
" > ${script_dir}/rnaseqc.lsf
  bsub < ${script_dir}/rnaseqc.lsf
fi
