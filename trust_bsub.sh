####################################################
# trust for pipeline
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1

    ################fastQC###############################
    script_dir=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)

    trust_script_path=${script_dir}/trust_lsf_script
    mkdir -p $trust_script_path

    input_table_file=$(cat $configFile | grep -w 'TRUST_INPUT_TABLE' | cut -d '=' -f2)
    output_dir=$(cat $configFile | grep -w 'TRUST_OUTPUT_PATH' | cut -d '=' -f2)
    python_module_version=$(cat $configFile | grep -w 'TRUST_PYTHON_MODULE_VERSION' | cut -d '=' -f2)

    mkdir -p $output_dir

    echo "looping through each bam file..."
    cat $input_table_file | while read line; do
        echo "$line"
        BAM=`basename $line`
        BAM=${BAM/.bam/}
outputdir_bam=$output_dir/${BAM}
mkdir -p $outputdir_bam

    echo "#BSUB -J trust_${BAM}
#BSUB -W 12:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/trust_${BAM}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/trust_${BAM}.e 
#BSUB -q medium
#BSUB -n 2
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

module load $python_module_version

cd $outputdir_bam
trust -f $line -I 200 -g hg19 -a
" > ${trust_script_path}/trust_${BAM}.lsf
    bsub < ${trust_script_path}/trust_${BAM}.lsf
done
fi
