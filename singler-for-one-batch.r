## 19-03-12 11:37:08
## (What it does) run single for one part(batch) of cells
## (to manuscript, method or conclusion)
## (To save life, please try to finish within 150 lines)
rm(list = ls());
library(readr)
myargs = commandArgs(trailingOnly=TRUE)
outputD=myargs[5]
if(!dir.exists(outputD)) dir.create(outputD)
setwd(outputD)
if(length(myargs) != 5 ) stop('check nargs ')
inputFile=myargs[1];
print(inputFile)
ncore=myargs[2];
istart=myargs[3];
iend=myargs[4];
print(ncore)
print(istart)
print(iend)
##run singleR
library(SingleR);library(Seurat)
seuratobj = readRDS(inputFile)
if(seuratobj@version < 3) seuratobj = UpdateSeuratObject(seuratobj)
data0 = GetAssayData(seuratobj, slot = 'counts')
datai = as.matrix(data0[,istart:iend,drop = F])
singler = CreateSinglerSeuratObject(datai,project.name='test',numCores=ncore)
res = singler$singler[[1]]$SingleR.single$labels
res = as.data.frame(res)
res.df = data.frame(ID = rownames(res), res,stringsAsFactors = F)
resFname = paste0('singler-',istart,'-',iend,'.tsv')
write_tsv(res.df, resFname)


