###################################################
# bsub sample script in a directory
# inputs: 
#   directory where sample scripts resides in
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
echo " usage: script_dir";
else
script_dir=$1
scripts_LIST=($(ls $script_dir/*.sh))

				for line in "${scripts_LIST[@]}"
				do
				c=0
				echo "$line"
				let c=c+1
				bam=$line
				script=$line
				samplename=`basename $bam`
				samplename=${samplename/.sh/}
				echo "#BSUB -J MM_${samplename}_vdjer
#BSUB -W 6:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/MM_${samplename}_vdjer.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/MM_${samplename}_vdjer.e 
#BSUB -cwd /rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/ 
#BSUB -q medium
#BSUB -n  12
#BSUB -M 204800
#BSUB -R rusage[mem=204800]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

echo \"submitting ${samplename}_vdjer jobs\"
sh $script" >  ${script_dir}/${samplename}.lsf
bsub < ${script_dir}/$samplename.lsf
sleep 2
done
fi

