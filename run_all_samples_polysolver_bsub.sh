####################################################
# polysolver pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create polysolver_mapping_sample_scripts subdir..."
    polysolver_mapping_sample_scripts_path=${SCRIPTS_PATH}/polysolver_mapping_sample_scripts
    mkdir -p $polysolver_mapping_sample_scripts_path

    ##polysolver will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'POLYSOLVER_SAMPLE_LIST_FILE' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        bam=$line
        samplename=`basename $bam`
        samplename=${samplename/.bam/}
        echo "#BSUB -J ${samplename}_polysolver
#BSUB -W 12:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${samplename}_polysolver.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${samplename}_polysolver.e 
#BSUB -q medium
#BSUB -n  9
#BSUB -M 204800
#BSUB -R rusage[mem=204800]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

module load polysolver/1.0
module load samtools/1.4
module load jdk/1.8.0_45

sh ${SCRIPTS_PATH}/polysolver_for_one_sample.sh $bam ${SCRIPTS_PATH}/config/tool_info.txt" >  ${polysolver_mapping_sample_scripts_path}/polysolver_${samplename}.lsf
        bsub <  ${polysolver_mapping_sample_scripts_path}/polysolver_${samplename}.lsf 
        sleep 2
    done

fi
