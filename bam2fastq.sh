####################################################
# bam to (pair) fastq for one bamfile
# inputs: 
#    bamfile(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
echo " usage: bamfile configFile";
else
bamfile=$1
configFile=$2

################bam2fastq###############################
output_dir=$(cat $configFile | grep -w 'BAM2FASTQ_OUTPUT_PATH' | cut -d '=' -f2)
java_mem=$(cat $configFile | grep -w 'BAM2FASTQ_JAVA_MEM' | cut -d '=' -f2)
JAVA_BIN=$(cat $configFile | grep -w 'BAM2FASTQ_JAVA_BIN' | cut -d '=' -f2)
JAR_PICARD=$(cat $configFile | grep -w 'BAM2FASTQ_PICARD_JAR' | cut -d '=' -f2)

sample=`basename ${bamfile}`
sample=${sample/.bam/}
output_dir1=${output_dir}/${sample}

mkdir -p $output_dir1
cd $output_dir1

$JAVA_BIN -jar $JAR_PICARD SamToFastq I=$bamfile FASTQ=${sample}_R1.fq SECOND_END_FASTQ=${sample}_R2.fq
gzip -f ${sample}_R1.fq
gzip -f ${sample}_R2.fq
fi
