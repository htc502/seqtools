####################################################
# star 2pass for one sample including unmapped reads
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
fq1=$1
fq2=$2
configFile=$3

genomeDir=$(cat $configFile | grep -w 'STAR2PASS_GENOME_DIR' | cut -d '=' -f2)
genomeRef=$(cat $configFile | grep -w 'STAR2PASS_GENOME_REF_FILE' | cut -d '=' -f2)
NTHREADS=$(cat $configFile | grep -w 'STAR2PASS_NTHREAD' | cut -d '=' -f2)
round1Index=$(cat $configFile | grep -w 'STAR2PASS_ROUND1_INDEX' | cut -d '=' -f2)
samplename=`basename $fq1`
sjdbOverhang=$(cat $configFile | grep -w 'STAR2PASS_SJDBOVERHANG' | cut -d '=' -f2)
samplename=${samplename/[.|_]R1*gz/}
genomeDir_pass1=$genomeDir/${samplename}_pass1

##mkdir -p $genomeDir_pass1
##cd $genomeDir_pass1
##STAR --runMode genomeGenerate --genomeDir $genomeDir_pass1 --genomeFastaFiles $genomeRef  --runThreadN $NTHREADS

r1Dir=$(cat $configFile | grep -w 'STAR2PASS_ROUND1_DIR' | cut -d '=' -f2)
r1Dir=$r1Dir/${samplename}
mkdir -p $r1Dir
cd $r1Dir
## to save time(~20min), we build pass 1 index once for all, !!!check the path if the enviroment changed!!!
STAR --genomeDir $round1Index --readFilesCommand zcat  --readFilesIn $fq1 $fq2 --runThreadN $NTHREADS  --outFileNamePrefix $r1Dir/${samplename}.

genomeDir_pass2=$genomeDir/${samplename}_pass2
mkdir -p  $genomeDir_pass2
cd $genomeDir_pass2
STAR --runMode genomeGenerate --genomeDir $genomeDir_pass2 --genomeFastaFiles $genomeRef --sjdbFileChrStartEnd $r1Dir/${samplename}.SJ.out.tab --sjdbOverhang ${sjdbOverhang} --runThreadN $NTHREADS

r2Dir=$(cat $configFile | grep -w 'STAR2PASS_ROUND2_DIR' | cut -d '=' -f2)
r2Dir=$r2Dir/$samplename
mkdir -p $r2Dir
cd $r2Dir
STAR --genomeDir $genomeDir_pass2 --readFilesCommand zcat  --readFilesIn $fq1 $fq2 --runThreadN $NTHREADS --outFileNamePrefix $r2Dir/${samplename}.   --outSAMunmapped Within
JAVA_BIN=$(cat $configFile | grep -w 'STAR2PASS_JAVA_BIN' | cut -d '=' -f2)
JAR_PICARD=$(cat $configFile | grep -w 'STAR2PASS_PICARD_JAR' | cut -d '=' -f2)
echo $JAVA_BIN
$JAVA_BIN -jar $JAR_PICARD AddOrReplaceReadGroups I=$r2Dir/${samplename}.Aligned.out.sam O=$r2Dir/${samplename}_rg_added_sorted.bam SO=coordinate RGID=id RGLB=library RGPL=platform RGPU=machine RGSM=sample 
$JAVA_BIN -jar $JAR_PICARD MarkDuplicates I=$r2Dir/${samplename}_rg_added_sorted.bam O=$r2Dir/${samplename}.star_marked.bam  CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT M=output.metrics 
##the bai index is not suitable for htseqcount...
mv $r2Dir/${samplename}.star_marked.bai $r2Dir/${samplename}.star_marked.bam.bai
if [ -f $r1Dir/${samplename}.Aligned.out.sam ];then
rm $r1Dir/${samplename}.Aligned.out.sam
fi
if [ -f $r2Dir/${samplename}.Aligned.out.sam ];then
rm $r2Dir/${samplename}.Aligned.out.sam
fi
##rm -rf $genomeDir_pass1
rm -rf $genomeDir_pass2
fi
