####################################################
# vdjer for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

#!/bin/sh
if [ $# != 3 ]; then
    echo " usage: samplebam chain configFile";
else
    bam=$1
    chain=$2
    configFile=$3
    vdjer_bin=$(cat $configFile | grep -w 'VDJER_BIN' | cut -d '=' -f2)
    vdjer_index=$(cat $configFile | grep -w 'VDJER_REF_DIR' | cut -d '=' -f2)
    nthread=$(cat $configFile | grep -w 'VDJER_NTHREAD' | cut -d '=' -f2)
    outputdir=$(cat $configFile | grep -w 'VDJER_OUTPUT_DIR' | cut -d '=' -f2)
    ins=$(cat $configFile | grep -w 'VDJER_INS' | cut -d '=' -f2)
    samplename=`basename $bam`
    samplename=${samplename/.bam/}

    mkdir -p ${outputdir}/${samplename}_${chain}
    cd ${outputdir}/${samplename}_${chain}
    ${vdjer_bin} --in $bam --t $nthread --ins $ins --chain $chain --ref-dir $vdjer_index/igh >${outputdir}/${samplename}_${chain}/${samplename}_${chain}.sam 2>${outputdir}/${samplename}_${chain}/${samplename}_${chain}.vdjer.log
fi
