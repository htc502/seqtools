###################################################
# gatk snp dnaseq pipeline for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/bash

if [ $# != 1 ]; then ## $# doesn't count the script name
echo " usage: configFile";
else
c=0
echo "$c"
configFile=$1
SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
echo "create gatk_snp_dnaseq_sample_scripts subdir in $SCRIPTS_PATH..."
GATK_sample_scripts_path=$(cat $configFile | grep -w 'GATK_SAMPLE_SCRIPT_PATH' | cut -d '=' -f2)
mkdir -p ${GATK_sample_scripts_path}
BAM_LIST=$(cat $configFile | grep -w 'BAM_LIST' | cut -d '=' -f2)
echo "looping through each bam file..."
cat $BAM_LIST | while read line; do
echo "$line"
BAM=${line/.bam/}
let c=c+1
while [ " " == " " ]
do
d=`ps -aux | grep 'gatk_snp_dnaseq_for_one_sample' | wc -l`
#echo "line=$line"
if [ $d -le 8 ]
then
echo "$c=$line"
echo "-------------------------------"
echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh
sh ${SCRIPTS_PATH}/gatk_snp_dnaseq_for_one_sample.sh $BAM  ${SCRIPTS_PATH}/config/tool_info.txt " > ${GATK_sample_scripts_path}/GATK_snp_dnaseq_${BAM}.sh
chmod 755 ${GATK_sample_scripts_path}/GATK_snp_dnaseq_${BAM}.sh
sh ${GATK_sample_scripts_path}/GATK_snp_dnaseq_${BAM}.sh &
sleep 1
break
else
sleep 30
fi
done
echo "end"
done
fi

