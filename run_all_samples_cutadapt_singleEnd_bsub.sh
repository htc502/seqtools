####################################################
# cutadapt: cutadapt for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create cutadapt_sample_scripts subdir..."
    cutadapt_sample_scripts_path=${SCRIPTS_PATH}/cutadapt_rawdata_sample_scripts
    mkdir -p ${cutadapt_sample_scripts_path}
    FASTQ_LIST=$(cat $configFile | grep -w 'FASTQ_LIST_FILE' | cut -d '=' -f2)
    cat $FASTQ_LIST | while read line; do
        echo "$line"
        fields=( $line )
        fq1=${fields[0]}
            FASTQ=`basename ${fq1}`
            samplename=${FASTQ/[.|_]R1*gz/}
            echo "#BSUB -J cutadapt_${samplename}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/cutadapt_${samplename}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/cutadapt_${samplename}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N

sh ${SCRIPTS_PATH}/cutadapt_for_one_sample_singleEnd.sh $fq1 ${SCRIPTS_PATH}/config/tool_info.txt " > ${cutadapt_sample_scripts_path}/cutadapt_rawdata_${samplename}.lsf
            bsub< ${cutadapt_sample_scripts_path}/cutadapt_rawdata_${samplename}.lsf
            sleep 2
    done

fi
