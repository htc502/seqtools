#BSUB -J vdjer_postproc
#BSUB -W 3:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/vdjer_postproc.o
#BSUB -eo /rsrch2/genomic_med/ghan1/log/vdjer_postproc.e 
#BSUB -cwd  /rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/vdjer_output 
#BSUB -q medium
#BSUB -n  13
#BSUB -M 65536
#BSUB -R rusage[mem=65536]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org


cd /rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/vdjer_output

##Gather all predicted sequences into a single fasta file
cat `find  *_IGK -name "vdj_contigs.fa"` |grep -v ">" | sort | uniq | cat -n | awk '{print ">seq_" $1 "\n" $2}' >all_sequences_igk.fa
cat `find  *_IGH -name "vdj_contigs.fa"` |grep -v ">" | sort | uniq | cat -n | awk '{print ">seq_" $1 "\n" $2}' >all_sequences_igh.fa
cat `find  *_IGL -name "vdj_contigs.fa"` |grep -v ">" | sort | uniq | cat -n | awk '{print ">seq_" $1 "\n" $2}' >all_sequences_igl.fa

postproc_script_dir=/rsrch2/genomic_med/ghan1/tools/vdjer/github/vdjer/post_process

SEQUENCE_FASTA=all_sequences_igh.fa

cat $SEQUENCE_FASTA | python $postproc_script_dir/get_cseq.py > vdj_const.fa

module load star/2.5.2b
module load samtools/1.4 
STAR \
    --runThreadN 8 \
    --genomeDir /rsrch2/genomic_med/ghan1/tools/STAR_hg38_index \
    --readFilesIn vdj_const.fa \
    --outSAMunmapped Within \
    --sjdbGTFfile /rsrch2/genomic_med/ghan1/tools/vdjer/github/vdjer/post_process/ucsc_hg38_knownGene.gtf  \
    --sjdbOverhang 100 \
    --outStd SAM | samtools view -1 -bS - > vdj_const.bam
2> star1.log

samtools sort vdj_const.bam vdj_const.sort
samtools index vdj_const.sort.bam 
samtools view vdj_const.sort.bam chr14 2> st.err | python $postproc_script_dir/call_isotypes.py $postproc_script_dir/igh_constant_func.bed > isotypes.txt



# Gather various vdjer statistics and cluster results.
#
# This step requires a specific directory hierarchy of
# <result_dir>/<sample>/<chain>/vdjer/<vdjer_and_rsem_output_here>
# For example: /datastore/nextgenout4/seqware-analysis/lmose/gcp/studies/anti_pd1/results/GSM2069823/igh/vdjer

# Sequence fasta from first step
SEQUENCES=all_sequences_igh.fa
# Output of isotype calling.  Can be set to no for light chains.
ISOTYPES=isotypes.txt
# List of sample names.  Entries here should match <sample> in the directory hierarchy
INVENTORY=final_inventory.txt
# Directory containing sample directories
RESULT_DIR=/rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/vdjer_output
# Path to VQuest summary file
VQUEST=/rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/vdjer_output/sequences_annotation/igh/1_Summary.txt
# Chain - needs to match <chain> in the directory hierarchy above
CHAIN=IGH
# Path to file containing read counts for each sample in format <sample_name>tab<count>
# Distinct mapped reads can be calculated using:  samtools view -F 0x104 <bam_file> | wc -l
READ_COUNTS=/rsrch2/genomic_med/ghan1/multiple_myeloma_analysis/vdjer_output/sample_read_counts.txt

python $postproc_script_dir/collect_vdjer_stats.py $SEQUENCES $ISOTYPES $INVENTORY $RESULT_DIR $VQUEST $CHAIN $READ_COUNTS | python $postproc_script_dir/cluster_results.py > cluster.res.igh.txt



