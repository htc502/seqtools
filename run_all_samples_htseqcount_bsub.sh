####################################################
# rsem+star pipeline: htseqcount for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/bash

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    echo "$c"
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create htseqcount_bam_sample_scripts subdir in $SCRIPTS_PATH..."
    htseqcount_sample_scripts_path=${SCRIPTS_PATH}/htseqcount_sample_scripts
    mkdir -p ${htseqcount_sample_scripts_path}
    BAM_LIST=$(cat $configFile | grep -w 'HTSEQCOUNT_BAM_LIST' | cut -d '=' -f2)
    echo "looping through each bam file..."
    njob=14
    cat $BAM_LIST | while read line; do
        echo "$line"
        BAM=`basename $line`
        BAM=${BAM/.bam/}
                echo "#BSUB -J htseqct_${BAM}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/htseqct_${BAM}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/htseqct_${BAM}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 65536
#BSUB -R rusage[mem=65536]
#BSUB -B
#BSUB -N

echo \"submitting htseqct_${BAM} jobs\"
##module load htseq/0.6.1
##0.9.1 has feature to allow us overcome memory overflow when dealing iwth coordinate sorted data...
module load htseq/0.9.1
module load samtools/1.1

sh ${SCRIPTS_PATH}/htseqcount_for_one_sample.sh $line ${SCRIPTS_PATH}/config/tool_info.txt " > ${htseqcount_sample_scripts_path}/htseqcount_bam_${BAM}.lsf
                bsub< ${htseqcount_sample_scripts_path}/htseqcount_bam_${BAM}.lsf
                sleep 1
    done
fi
