####################################################
# star 2pass for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
fq1=$1
fq2=$2
configFile=$3

genomeDir=$(cat $configFile | grep -w 'STAR2PASS_GENOME_DIR' | cut -d '=' -f2)
genomeRef=$(cat $configFile | grep -w 'STAR2PASS_GENOME_REF_FILE' | cut -d '=' -f2)
NTHREADS=$(cat $configFile | grep -w 'STAR2PASS_NTHREAD' | cut -d '=' -f2)
round1Index=$(cat $configFile | grep -w 'STAR2PASS_ROUND1_INDEX' | cut -d '=' -f2)
samplename=`basename $fq1`
sjdbOverhang=$(cat $configFile | grep -w 'STAR2PASS_SJDBOVERHANG' | cut -d '=' -f2)
samplename=${samplename/[.|_]R1*gz/}
genomeDir_pass1=$genomeDir/${samplename}_pass1


if [ ! -d $genomeDir ];then
echo "tmp dir: $genomeDir doesn't exist; going to create one"
mkdir -p $genomeDir
fi


##stop if reference file doesn't exsit
if [ ! -f $genomeRef ];then
echo "$genomeRef doesn't exist"
exit
fi

# module load STAR/2.4.2a (load this in run_all_samples script)
##mkdir -p $genomeDir_pass1
##cd $genomeDir_pass1
##STAR --runMode genomeGenerate --genomeDir $genomeDir_pass1 --genomeFastaFiles $genomeRef  --runThreadN $NTHREADS

r1Dir=$(cat $configFile | grep -w 'STAR2PASS_ROUND1_DIR' | cut -d '=' -f2)
r1Dir=$r1Dir/${samplename}
mkdir -p $r1Dir
cd $r1Dir
echo "first round mapping"
## to save time, we build pass 1 index once for all, !!!check the path if the enviroment changed!!!
STAR --genomeDir $round1Index  \
     --readFilesIn $fq1 $fq2 \
     --runThreadN $NTHREADS \
     --outFileNamePrefix $r1Dir/${samplename}.  \
     --outFilterMultimapScoreRange 1  \
     --outFilterMultimapNmax 20   \
     --outFilterMismatchNmax 10    \
     --alignIntronMax 500000    \
     --alignMatesGapMax 1000000   \
     --sjdbScore 2    \
     --alignSJDBoverhangMin 1   \
     --genomeLoad NoSharedMemory     \
     --readFilesCommand zcat     \
     --outFilterMatchNminOverLread 0.33   \
     --outFilterScoreMinOverLread 0.33    \
     --sjdbOverhang $sjdbOverhang \
     --outSAMstrandField intronMotif    \
     --outSAMtype None   \
     --outSAMmode None

genomeDir_pass2=$genomeDir/${samplename}_pass2
mkdir -p  $genomeDir_pass2
cd $genomeDir_pass2
echo "regenerate intermediate genome reference"
STAR --runMode genomeGenerate \
     --genomeDir $genomeDir_pass2   \
     --genomeFastaFiles $genomeRef   \
     --sjdbFileChrStartEnd $r1Dir/${samplename}.SJ.out.tab   \
     --sjdbOverhang ${sjdbOverhang} \
     --runThreadN $NTHREADS

r2Dir=$(cat $configFile | grep -w 'STAR2PASS_ROUND2_DIR' | cut -d '=' -f2)
r2Dir=$r2Dir/$samplename
mkdir -p $r2Dir
cd $r2Dir
echo "second round mapping"
STAR --genomeDir $genomeDir_pass2 \
     --readFilesIn $fq1 $fq2  \
     --runThreadN $NTHREADS \
     --outFileNamePrefix $r2Dir/${samplename}.  \
     --outFilterMultimapScoreRange 1   \
     --outFilterMultimapNmax 20    \
     --outFilterMismatchNmax 10    \
     --alignIntronMax 500000     \
     --alignMatesGapMax 1000000   \
     --sjdbScore 2    \
     --alignSJDBoverhangMin 1 \
     --genomeLoad NoSharedMemory \
     --limitBAMsortRAM 0  \
     --readFilesCommand zcat  \
     --outFilterMatchNminOverLread 0.33   \
     --outFilterScoreMinOverLread 0.33    \
     --sjdbOverhang $sjdbOverhang \
     --outSAMstrandField intronMotif \
     --outSAMattributes NH HI NM MD AS XS \
     --outSAMunmapped Within  \
     --outSAMtype BAM SortedByCoordinate \
     --outSAMheaderHD @HD VN:1.4  \
     --outSAMattrRGline ID:id LB:lib SM:sample PL:platform 

##mv $r2Dir/${samplename}.star_marked.bai $r2Dir/${samplename}.star_marked.bam.bai
if [ -f $r1Dir/${samplename}.Aligned.out.sam ];then
rm $r1Dir/${samplename}.Aligned.out.sam
fi
if [ -f $r2Dir/${samplename}.Aligned.out.sam ];then
rm $r2Dir/${samplename}.Aligned.out.sam
fi
##rm -rf $genomeDir_pass1
rm -rf $genomeDir_pass2
fi
