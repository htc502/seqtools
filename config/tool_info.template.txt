[pipeline-config]
##scripts path
SCRIPTS_PATH=

##keep for compacity
[rsem-star-config]
## Reference Files
STAR_REF=
## Tool Paths
FASTQC_PATH=
RNASEQC_JAR=
CUTADAPT_PATH=
RSEM_PATH=
STAR_PATH=
JAVA_PATH=
JAVA_BIN=
PICARD_PATH=
SAMTOOLS_PATH=

## Tool Parameters
CUTADAPT_PARAMS=
RSEM_PARAMs=

##run info
##put fq files names into this file, one name per line; fqs in this file will be feed to the pipeline
##suffix .fq is required instead of .fastq
[fastqc-config]
FASTQC_PATH=
FASTQ_LIST=
##in case you start from bam files
BAM_LIST=
##set fastq files location(file path)
FASTQC_INPUT_PATH=
##set fastqc threads
FASTQC_NTHREADS=
##our script generate intermidiate scripts for later check, store them here
TMP_SCRIPT_PATH=
##
BAM_INPUT_PATH=
##set the storage location of fastqc results
FASTQC_OUTPUT_PATH=
##summary output
FASTQC_OUTPUT_SUMMARY_PATH=

[rnaseqc-config]
##set the storage location of rnaseqc results
RNASEQC_OUTPUT_PATH=
##input bam file table(rnaseqc allows table as input)
RNASEQC_INPUT_TABLE=
##bwa path(tools like rnaseqc use it)
BWA_PATH=
##rRNA fasta reference(used by rnaseqc for rRNA level estimation)
RRNA_REF_FILE=
##gtf file 
GTF_FILE=
##rnaseqc reference genome
RNASEQC_REF_GENOME=

##htseq-count
[htseq-count-config]
HTSEQCOUNT_GFF_FILE=
HTSEQCOUNT_SCRIPT_PATH=
HTSEQCOUNT_OUTPUT_PATH=

[cutadapt-config]
##set the location of input fq files for cutadaptor
CUTADAPT_INPUT_PATH=
##set the location for output fq files of cutadaptor
CUTADAPT_OUTPUT_PATH=
##after cutadaptor,we do quality control again to make sure everything is alright
FASTQC_AFTER_CUTADAPT_OUTPUT_PATH=
FASTQC_AFTER_CUTADAPT_INPUT_PATH=
##set reads mapping output filepath
RSEM_OUTPUT_PATH=

##gatk
[gatk-config]
GATK_SAMPLE_SCRIPT_PATH=
GATK_JAVA_BIN=
GATK_JAVA_MEM=
GATK_JAR=
PICARD_JAR=
GATK_REF_GENOME=
GATK_KNOW_SNP_VCF=
GATK_BAM_INPUT_PATH=
GATK_DNASEQ_BAM_INPUT_PATH=
GATK_DNASEQ_OUTPUT_PATH=
GATK_OUTPUT_PATH=

##polysolver
[polysolver-config]
POLYSOLVER_OUTPUT_PATH=
POLYSOLVER_SAMPLE_LIST_FILE=
POLYSOLVER_NTHREADS=
