## 18-12-27 12:06:10
## (What it does) bsub script for gsva-v1.r 
## (to manuscript, method or conclusion)
## (To save life, please try to finish within 150 lines)
#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create lsf scripts subdir..."
    lsf_scripts_path=${SCRIPTS_PATH}/lsf_scripts
    mkdir -p ${lsf_scripts_path}
    ncell=$(cat $configFile | grep -w 'GSVA_Ncells' | cut -d '=' -f2)
    nsegLen=$(cat $configFile | grep -w 'GSVA_NsegLen' | cut -d '=' -f2)
    ncore=$(cat $configFile | grep -w 'GSVA_Ncore' | cut -d '=' -f2)
    outputD=$(cat $configFile | grep -w 'GSVA_OUTPUTD' | cut -d '=' -f2)
    gmtFile=$(cat $configFile | grep -w 'GSVA_GMTFILE' | cut -d '=' -f2)
    seurat3ObjFile=$(cat $configFile | grep -w 'GSVA_seuratOBJ_FILE' | cut -d '=' -f2)
    istart=1
    while [[  $istart -le $ncell ]];do
	echo "$istart"
	iend=$((istart+nsegLen-1))
	if (( iend > ncell)); then
	    iend=$ncell
	fi
	scriptname=${istart}_${iend}
	echo "#BSUB -J gsva_${scriptname}
#BSUB -W 2:00
#BSUB -o /rsrch2/genomic_med/ghan1/log/ssgsva_${scriptname}.o 
#BSUB -e /rsrch2/genomic_med/ghan1/log/ssgsva_${scriptname}.e 
#BSUB -q short
#BSUB -n  $ncore
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N
module load R/3.5.0-shlib
Rscript --no-save ${SCRIPTS_PATH}/gsva-v1-mouse.r $istart $iend $ncore $outputD $gmtFile $seurat3ObjFile" > ${lsf_scripts_path}/ssgsva_${scriptname}.lsf
	bsub< ${lsf_scripts_path}/ssgsva_${scriptname}.lsf
	sleep 2
	istart=$((istart+nsegLen))
    done

fi
