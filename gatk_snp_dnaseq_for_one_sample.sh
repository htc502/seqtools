####################################################
# gatk rnaseq vairant calling pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
echo " usage: sampleName configFile";
else
sample=$1
configFile=$2
JAVA_BIN=$(cat $configFile | grep -w 'GATK_JAVA_BIN' | cut -d '=' -f2)
java_mem=$(cat $configFile | grep -w 'GATK_JAVA_MEM' | cut -d '=' -f2)
GATK_jar=$(cat $configFile | grep -w 'GATK_JAR' | cut -d '=' -f2)
PICARD_jar=$(cat $configFile | grep -w 'PICARD_JAR' | cut -d '=' -f2)
reference_file=$(cat $configFile | grep -w 'GATK_REF_GENOME' | cut -d '=' -f2)
##to add
input_dir=$(cat $configFile | grep -w 'GATK_DNASEQ_BAM_INPUT_PATH' | cut -d '=' -f2)
##to add
output_dir=$(cat $configFile | grep -w 'GATK_DNASEQ_OUTPUT_PATH' | cut -d '=' -f2)
GATK_KNOW_SNP_VCF=$(cat $configFile | grep -w 'GATK_KNOW_SNP_VCF' | cut -d '=' -f2)

bamfile=${sample}.bam

cd ${output_dir}
echo $java_mem
echo "run gatk snp calling(RNAseq) on ${bamfile}"
##Add read groups, sort, mark duplicates, and create index
##java -jar picard.jar AddOrReplaceReadGroups I=star_output.sam O=rg_added_sorted.bam SO=coordinate RGID=id RGLB=library RGPL=platform RGPU=machine RGSM=sample 
##java -jar picard.jar MarkDuplicates I=rg_added_sorted.bam O=dedupped.bam  CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT M=output.metrics 

##Split'N'Trim and reassign mapping qualities
##${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SplitNCigarReads -R ${reference_file} -I ${input_dir}/${bamfile} -o  ${output_dir}/${sample}.split.bam -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS

##build index...make sure that input_dir is able to write
##${JAVA_BIN} -Xmx${java_mem} -jar ${PICARD_jar} BuildBamIndex INPUT=${input_dir}/${sample}.bam 

##indel realignment 
##${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T RealignerTargetCreator -R ${reference_file} -I ${input_dir}/${sample}.bam -o ${output_dir}/${sample}.realignment_targets.list

##${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T IndelRealigner -R ${reference_file} -I ${input_dir}/${sample}.bam -targetIntervals ${output_dir}/${sample}.realignment_targets.list -o ${output_dir}/${sample}.realigned_reads.bam

##variant calling
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T HaplotypeCaller -R ${reference_file} -I ${output_dir}/${sample}.realigned_reads.bam -o ${output_dir}/${sample}.vcf

##extrac snps only(can also add indels)
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SelectVariants -R ${reference_file} -V ${output_dir}/${sample}.vcf -selectType SNP -o ${output_dir}/${sample}.snps.vcf
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SelectVariants -R ${reference_file} -V ${output_dir}/${sample}.vcf -selectType INDEL -o ${output_dir}/${sample}.indelss.vcf

##filter
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T VariantFiltration -R ${reference_file} -V ${output_dir}/${sample}.snps.vcf --filterExpression 'QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0 || SOR > 4.0' --filterName "basic_snp_filter" -o ${output_dir}/${sample}.filtered_snps.vcf
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T VariantFiltration -R ${reference_file} -V -o ${output_dir}/${sample}.indelss.vcf --filterExpression 'QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0 || SOR > 10.0' --filterName "basic_indel_filter" -o ${output_dir}/${sample}.filtered_indels.vcf

##BQSR
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T BaseRecalibrator -R ${reference_file} -I ${output_dir}/${sample}.realigned_reads.bam  -knownSites filtered_snps.vcf -knownSites filtered_indels.vcf -o ${output_dir}/${sample}.recal_data.table
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T BaseRecalibrator -R ${reference_file} -I ${output_dir}/${sample}.realigned_reads.bam  -knownSites filtered_snps.vcf -knownSites filtered_indels.vcf -BQSR recal_data.table -o ${output_dir}/${sample}.post_recal_data.table
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T AnalyzeCovariates -R ${reference_file} -before ${output_dir}/${sample}.recal_data.table -after ${output_dir}/${sample}.post_recal_data.table -plots ${output_dir}/${sample}.recalibration_plots.pdf

${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T PrintReads -R ${reference_file} -I ${output_dir}/${sample}.realigned_reads.bam -BQSR ${output_dir}/${sample}.recal_data.table -o ${output_dir}/${sample}.recal_reads.bam
##call variants
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T HaplotypeCaller -R ${reference_file} -I ${output_dir}/${sample}.recal_reads.bam -o ${output_dir}/${sample}.raw_variants_recal.vcf

##snps and indels extraction
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SelectVariants -R ${reference_file} -V ${output_dir}/${sample}.raw_variants_recal.vcf -selectType SNP -o ${output_dir}/${sample}.raw_snps_recal.vcf

${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SelectVariants -R ${reference_file} -V ${output_dir}/${sample}.raw_variants_recal.vcf -selectType INDEL -o ${output_dir}/${sample}.raw_indels_recal.vcf

##filtration
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T VariantFiltration -R ${reference_file} -V ${output_dir}/${sample}.raw_snps_recal.vcf --filterExpression 'QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0 || SOR > 4.0' --filterName "basic_snp_filter" -o ${output_dir}/${sample}.filtered_snps_final.vcf

${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T VariantFiltration -R ${reference_file} -V ${output_dir}/${sample}.raw_indels_recal.vcf --filterExpression 'QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0 || SOR > 10.0' --filterName "basic_indel_filter" -o ${output_dir}/${sample}.filtered_indels_recal.vcf

fi
