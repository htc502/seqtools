##forgot about this. what we have now is bam aligned to genome.
##rsem requires bam files aligned to transcriptome to get fpkm rpkm quantification.
##consider merge fastq files and then do the alignment from the begining is a better way

module load STAR/2.4.2a

nthread=8
DNArefDir=/scratch/rists/hpcapps/reference/human/hg19/fastas/Homo_sapiens_assembly19.fasta
rsemTranscriptomeRef=/rsrch2/genomic_med/ghan1/tools/STAR_hg19_transcriptome_for_RSEM/STAR_hg19_transcriptome_for_RSEM
##build transcriptome reference of STAR bam once for all
rsem-prepare-reference --gff3 /rsrch2/genomic_med/ghan1/tools/RNA-SeQC/gencode.v19.annotation.rmchr.gff \
                       --star \
                       --star-path  /risapps/rhel6/STAR/2.4.2a/bin \
                       -p $nthread \
                       $DNArefDir
                       $rsemTranscriptomeRef

bamFile=/rsrch2/genomic_med/ghan1/PD1_inhibitor_miles_analysis/star_bam_final/bam_merged/884802.bam
outputDirPref=/rsrch2/genomic_med/ghan1/PD1_inhibitor_miles_analysis/star_bam_merge/rsem_output/884820
mkdir -p $outputDirPref
rsem-calculate-expression --paired-end \
                          --alignments \
                          -p $nthread  \
                          $bamFile \
                          $rsemTranscriptomeRef \
                          $outputDirPref
