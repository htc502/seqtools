####################################################
# this is cnvkit for ascat usage for one sample
#  inputs:
#     configFile (for reference, capture bed file, outputdir)
#     tumorbam
#     normalbam
#     tumorID
####################################################

if [ $# != 4 ]; then
    echo " usage: tumor.Bam normal.Bam outputSampleID configFile";
else
    tbam=$1
    nbam=$2
    tName=$3
    configFile=$4

    captureBed=$(cat $configFile | grep -w 'GATK4ASCAT_CAPTURE_BED_FILE' | cut -d '=' -f2)
    outputDir=$(cat $configFile | grep -w 'CNVKIT4ASCAT_OUTPUT_DIR' | cut -d '=' -f2)
    cnvkitOutputNameN=$outputDir/${tName}-matchedNormal
    cnvkitOutputNameT=$outputDir/$tName
    cnvkit.py coverage -p 10 $nbam $captureBed -o $cnvkitOutputNameN
    cnvkit.py coverage -p 10 $tbam $captureBed -o $cnvkitOutputNameT
fi
