####################################################
# mixcr for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

#!/bin/sh
if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
    fq1=$1
    fq2=$2
    configFile=$3
    outputdir=$(cat $configFile | grep -w 'MIXCR_OUTPUT_DIR' | cut -d '=' -f2)
    mixcr=$(cat $configFile | grep -w 'MIXCR_BIN' | cut -d '=' -f2)
    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R1*gz/}
    mkdir -p ${outputdir}/${samplename}
    cd $outputdir/$samplename
    $mixcr align -p rna-seq -r log.txt $fq1 $fq2 alignments.vdjca
    $mixcr assemblePartial alignments.vdjca alignments_contigs.vdjca
    $mixcr assemble -r log.txt alignments_contigs.vdjca clones.clns
    $mixcr exportClones clones.clns clones.txt
fi
