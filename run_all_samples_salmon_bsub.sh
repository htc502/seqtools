####################################################
# salmon pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create salmon_mapping_sample_scripts subdir..."
    salmon_mapping_sample_scripts_path=${SCRIPTS_PATH}/salmon_mapping_sample_scripts
    mkdir -p $salmon_mapping_sample_scripts_path

    ##salmon will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'SALMON_SAMPLE_LIST_FILE' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        fields=( $line )
        fq1=${fields[0]}
        fq2=${fields[1]}
        samplename=`basename $fq1`
        samplename=${samplename/[.|_]R1*gz/}
        echo "#BSUB -J ${samplename}_salmon
=======
#BSUB -W 12:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${samplename}_salmon.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${samplename}_salmon.e 
#BSUB -q medium
#BSUB -n  5
#BSUB -M 184320
#BSUB -R rusage[mem=184320]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org
sh ${SCRIPTS_PATH}/salmon_for_one_sample.sh $fq1 $fq2 ${SCRIPTS_PATH}/config/tool_info.txt" >  ${salmon_mapping_sample_scripts_path}/salmon_${samplename}.lsf
##        bsub < ${salmon_mapping_sample_scripts_path}/salmon_${samplename}.lsf
        sleep 2
    done

fi
