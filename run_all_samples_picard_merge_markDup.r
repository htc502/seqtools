####################################################
# picard merge and mark duplicates pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/Rscript
cargs = commandArgs(trailingOnly = T)
if(length(cargs) != 1) { ## $# doesn't count the script name
  print(" usage: configFile");
} else {
  configFile=cargs[1]
  cmd1=paste0("cat ",$configFile," | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2")
  SCRIPTS_PATH=system(command = cmd1,intern = T)
  print("create bwamem_mapping_sample_scripts subdir...")
  bwamem_mapping_sample_scripts_path=file.path(SCRIPTS_PATH,"bwamem_mapping_sample_scripts")
  dir.create(bwamem_mapping_sample_scripts_path)

  ##bwamem will only do QC on those samples which undergo cutadapt...
  cmd2=paste0("cat ",$configFile," | grep -w 'PICARD_BAM_MERGE_TABLE' | cut -d '=' -f2")
  MERGE_TABLE=system(command = cmd2, intern = T)

  tabl = reaLines(MERGE_TABLE) ##we keep header 
  for(ir in seq_along(tabl)) {
    line=tabl[ir]
    fields=strsplit(line,split = '\t')
    samplename=fields[1]
    mkdupOut=file.path(dirname(samplename),gsub('.bam','_markDup.bam',basename(samplename)))
    metricsOut=file.path(dirname(samplename),gsub('.bam','_dupMetrics.txt',basename(samplename)))
    bams2merge=fields[-1]
    cmd3 = paste0("#BSUB -J ",samplename,"_merge
#BSUB -W 6:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/",samplename,"_merge.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/",samplename,"_merge.e 
#BSUB -q medium
#BSUB -n  14
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N
module load picard/2.9.0
java -jar picard.jar MergeSamFiles \\
    ASSUME_SORTED=false \\
    CREATE_INDEX=true \\                 
",
paste0('    INPUT=',bams2merge,' \\ ',collapse = '\n')  ," 
    MERGE_SEQUENCE_DICTIONARIES=false \\
    OUTPUT= ",samplename ," \\
    SORT_ORDER=coordinate \\
    USE_THREADING=true \\
    VALIDATION_STRINGENCY=STRICT

java -jar picard.jar MarkDuplicates \\
CREATE_INDEX=true \\
INPUT=",samplename," \\
VALIDATION_STRINGENCY=STRICT
","O=",mkdupOut," \\
","M=",metricsOut)
    writeLines(cmd3,con=file.path(merge_mapping_sample_scripts_path,paste0("merge_",gsub('.bam','',basename(samplename)),".lsf")))
    cmd4=paste0("bsub <  ",file.path(merge_mapping_sample_scripts_path,paste0("merge_",gsub('.bam','',basename(samplename)),".lsf"))))
  system(cmd4) 
  Sys.sleep(2)
}
