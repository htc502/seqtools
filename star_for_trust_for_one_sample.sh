####################################################
# star 4trust for one sample trust (including unmapped reads; disbale local alignment; nmis <= 2)
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
fq1=$1
fq2=$2
configFile=$3

NTHREADS=$(cat $configFile | grep -w 'STAR4TRUST_NTHREAD' | cut -d '=' -f2)
round1Index=$(cat $configFile | grep -w 'STAR4TRUST_ROUND1_INDEX' | cut -d '=' -f2)
samplename=`basename $fq1`
sjdbOverhang=$(cat $configFile | grep -w 'STAR4TRUST_SJDBOVERHANG' | cut -d '=' -f2)
samplename=${samplename/[.|_]R1*gz/}

r1Dir=$(cat $configFile | grep -w 'STAR4TRUST_OUTPUT_PATH' | cut -d '=' -f2)
r1Dir=$r1Dir/${samplename}
mkdir -p $r1Dir
cd $r1Dir
## to save time(~20min), we build pass 1 index once for all, !!!check the path if the enviroment changed!!!
STAR --genomeDir $round1Index \
     --readFilesCommand zcat \
     --readFilesIn $fq1 $fq2 \
     --runThreadN $NTHREADS  --outFileNamePrefix $r1Dir/${samplename}. \
     --outSAMunmapped Within \
     --alignEndsType EndToEnd \
     --outFilterMismatchNmax 2

JAVA_BIN=$(cat $configFile | grep -w 'STAR4TRUST_JAVA_BIN' | cut -d '=' -f2)
JAR_PICARD=$(cat $configFile | grep -w 'STAR4TRUST_PICARD_JAR' | cut -d '=' -f2)

$JAVA_BIN -jar $JAR_PICARD AddOrReplaceReadGroups I=$r1Dir/${samplename}.Aligned.out.sam O=$r1Dir/${samplename}_rg_added_sorted.bam SO=coordinate RGID=id RGLB=library RGPL=platform RGPU=machine RGSM=sample 
$JAVA_BIN -jar $JAR_PICARD MarkDuplicates I=$r1Dir/${samplename}_rg_added_sorted.bam O=$r1Dir/${samplename}.star_marked.bam  CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT M=output.metrics 
##the bai index is not suitable for htseqcount...
mv $r1Dir/${samplename}.star_marked.bai $r1Dir/${samplename}.star_marked.bam.bai
if [ -f $r1Dir/${samplename}.Aligned.out.sam ];then
rm $r1Dir/${samplename}.Aligned.out.sam
fi
fi
