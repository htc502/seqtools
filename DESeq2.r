rm(list=ls())
args = commandArgs(trailingOnly = T)
if(!require(readr)) stop('erro loading readr')
print(length(args))
if(!(length(args)== 1 | length(args) == 2) ) stop('usage: Rscript DESeq2.r configFile [optional:featurecounts]')
configFile=args[1]
## set before running
htseq_root_dir = system(command = paste0("cat ",configFile," | grep -w 'HTSEQCOUNT_OUTPUT_PATH' | cut -d '=' -f2"),intern = T)
if(length(args) > 1) {
  featurecounts=args[2]
  if(featurecounts=='featurecounts') {
    htseq_root_dir = system(command = paste0("cat ",configFile," | grep -w 'FEATURECOUNTS_OUTPUT_PATH' | cut -d '=' -f2"),intern = T)
  } else {
    stop('usage: Rscript DESeq2.r configFile [optional:featurecounts]')
  }
}
sampleInfo = system(command = paste0("cat ",configFile," | grep -w 'DESEQ2_SAMPLE_INFO_PATH' | cut -d '=' -f2"),intern = T)
output_dir=htseq_root_dir

sample.info = read_tsv(sampleInfo)
countmtr = read_tsv(file.path(htseq_root_dir,'protein_coding_gene_countMatrix.tsv'))
##in case if sample.info only use a subset of samples...
if(!require(dplyr)) stop('error loading dplyr')
select_cols = c('SYMBOL','ID','Type',sample.info$Sample_ID)
countmtr = select(countmtr, one_of(select_cols))
cts = select(countmtr,-SYMBOL,-ID,-Type) %>% as.matrix
rownames(cts) = countmtr$SYMBOL
match(colnames(cts),sample.info$Sample_ID) -> pos
coldata = sample.info[pos, ]

if(!require(DESeq2 )) stop('error loading DESeq2')
dds.all <- DESeqDataSetFromMatrix(countData = cts,
                                  colData = coldata,
                                  design = ~ condition)
dds.all <- dds.all[ rowSums(counts(dds.all)) > 1, ] ## rm low expressed

dds.all <- DESeq(dds.all)
res <- results(dds.all)
res = tbl_df(data.frame(Gene=rownames(res),res)) %>%
  mutate(log10fdr = -log10(padj))

compName = strsplit(basename(sampleInfo),split = '\\.')[[1]][1]
write_tsv(res, path=file.path(output_dir,paste0(compName,'DESeq2_result.tsv')))
##make rnk file for gsea
write_tsv(data.frame(select(res, Gene, stat)),
          path = file.path(output_dir,
                           paste0(compName,'DESeq2_result_gsea.rnk')))

##make a heatmap and volcanno plot for checking purpose
topten = rep(NA, nrow(res))
## in deseq2 we use P(NR) as the reference group
up10 = arrange(filter(res,!is.na(pvalue)), desc(log2FoldChange))$Gene[1:10]
down10 = arrange(filter(res,!is.na(pvalue)), log2FoldChange)$Gene[1:10]
topten[res$padj < 0.1 & res$Gene %in% up10 ] = 'Up' ## this is up in R
topten[res$padj < 0.1 & res$Gene %in% down10 ] = 'Down'
topten[is.na(topten)] = 'N.S'
Show=rep('N',nrow(res))
Show[topten == 'Up' | topten == 'Down'] = 'Y'
res = data.frame(res, topten=topten,
                 Show=Show)
##volcano plot
library(ggrepel)
gpdat = res
gpdat = na.omit(gpdat)
ggplot(gpdat, aes(x = log2FoldChange, y = `log10fdr`)) +
  geom_point(aes(color = topten)) +
  geom_vline(xintercept=-1,linetype = 'dashed',color = 'blue') +
  geom_vline(xintercept=1,linetype = 'dashed',color = 'blue') + 
  geom_hline(yintercept=1,linetype = 'dashed',color = 'blue') + 
  scale_color_manual(values = c("red", "grey60",'blue')) +
  theme_bw(base_size = 12) + theme(legend.position = "bottom") +
  ylab('-log10FDR') +
  xlab('log2FoldChange') + 
  geom_text_repel(
    data = subset(gpdat, Show == 'Y'),
    aes(label = Gene),
    size = 5,
    box.padding = unit(0.35, "lines"),
    point.padding = unit(0.3, "lines")
  ) -> gp.volcano

##add label to inidcate the group
tmp.dat = data.frame(x = c(min(gpdat$log2FoldChange) + .1 * abs(min(gpdat$log2FoldChange)),max(gpdat$log2FoldChange) - .1 * max(gpdat$log2FoldChange)),
                     y = rep(max(gpdat$log10fdr) - .1 * max(gpdat$log10fdr),2),
                     text = levels(dds.all@colData$condition), stringsAsFactors = F)
gp.volcano = gp.volcano + geom_text(data = tmp.dat,
                                    aes(label = text, x = x, y = y),
                                    color = 'firebrick',
                                    size = 16)
ggsave(filename = file.path(output_dir,paste0(compName,'volcanoPlot.DEG.pdf')),gp.volcano,height = 12,width = 12)

mat = assay(rlog(dds.all))
mat.genes = rownames(mat)
selGenes = filter(res, Show == 'Y')$Gene
if(length(selGenes) > 3) {
  mat1 = mat[ mat.genes %in% selGenes, ]
  df = data.frame(tbl_df(dds.all@colData))
  df$condition = (as.character(df$condition))
  rownames(df) = df$Sample_ID
  library(pheatmap)
  library(RColorBrewer)
  sweep(mat1, 1, apply(mat1, 1, mean)) -> mat1
  mat1 = sweep(mat1, 1, FUN = '/',STATS = apply(mat1, 1 ,sd))
  pheatmap(mat1,
           scale = 'none',
           color = colorRampPalette(rev(brewer.pal(n=7,name = "RdYlGn")))(100),
           annotation_col=df[,c('condition'),drop = F],
           file = file.path(output_dir,paste0(compName,'DEG_row_norm_heatmap.pdf')))
} else {
  warnings('will not show heatmap because selGenes number < 3\n')
}
##for later usage(like heatmap etc)
save(dds.all,file= file.path(output_dir,paste0(compName,'DESeq2_obj.rda')))
