####################################################
# rna-seqc for pipeline
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1

	################fastQC###############################
	JAVA_BIN=$(cat $configFile | grep -w 'JAVA_BIN' | cut -d '=' -f2)
	rnaseqc_jar=$(cat $configFile | grep -w 'RNASEQC_JAR' | cut -d '=' -f2)
bwa_path=$(cat $configFile | grep -w 'BWA_PATH' | cut -d '=' -f2)
rrna_file=$(cat $configFile | grep -w 'RRNA_REF_FILE' | cut -d '=' -f2)
gtf_file=$(cat $configFile | grep -w 'GTF_FILE' | cut -d '=' -f2)
reference_file=$(cat $configFile | grep -w 'RNASEQC_REF_GENOME' | cut -d '=' -f2)

  input_table=$(cat $configFile | grep -w 'RNASEQC_INPUT_TABLE' | cut -d '=' -f2)
  output_dir=$(cat $configFile | grep -w 'RNASEQC_OUTPUT_PATH' | cut -d '=' -f2)

	mkdir -p $output_dir
	cd $output_dir

	echo "run RNASeQC ..."
	echo "$JAVA_BIN -jar $rnaseqc_jar -r ${reference_file} -bwa ${bwa_path} -BWArRNA ${rrna_file}  -o $output_dir -s ${input_table} -t $gtf_file -ttype 2  2>&1 |tee $output_dir/rnaseqc.log"
        $JAVA_BIN -jar $rnaseqc_jar -r ${reference_file} -bwa ${bwa_path} -BWArRNA ${rrna_file}  -o $output_dir -s ${input_table} -t $gtf_file 2>&1 |tee $output_dir/rnaseqc.log
fi
