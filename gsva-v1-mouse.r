## 18-12-27 11:12:47
## (What it does) gsva script
## (to manuscript, method or conclusion)
## (To save life, please try to finish within 150 lines)
rm(list = ls());library(readxl);library(readr);library(dplyr)
args = commandArgs(trailingOnly=TRUE)
print(paste(args,collapse = ';'))
if(length(args) != 6) stop('error start and stop parameters')
start = args[1];end = args[2];ncore=args[3]
outputD=args[4]
gmtFile=args[5]
seurat3ObjFile=args[6]
if(!dir.exists(outputD)) dir.create(outputD)
setwd(outputD)
library(GSVA)
library('GSEABase');
library('cogena');library(Seurat)
geneSets <- gmt2list(gmtFile);
seuratObj = readRDS(seurat3ObjFile)
uniqGenes = unique(sort(rownames(seuratObj)))

baseD = outputD
##covert mouse symbol to human
getDb.m2h = function() {
    ## for manually update database
    oldwd = getwd()
    setwd(baseD)
    downLoadDb = function() {
        download.file('ftp://ftp.ncbi.nih.gov/pub/HomoloGene/current/homologene.data',
                      destfile = 'homologene.data')
    }

    if(!file.exists(file.path(baseD,'homologene.data'))) downLoadDb()

    dat = read_tsv(file.path(baseD,'homologene.data'),col_names = F)
    colnames(dat) = c('hID','taxID','GeneID','Symbol',
                      'proteingi','proteinAcc')
    dat = dat %>% dplyr::select(hID, taxID, Symbol) %>%
        dplyr::filter(taxID %in% c(9606, 10090)) 
    ## remove duplicates in hID.taxID
    tmp = group_by(dat, taxID, hID) %>% summarize(n = n()) %>% ungroup() %>%
        group_by(hID) %>% summarize(n = n())
    badhIDs = filter(tmp, n == 1) %>% .$hID
    dat = filter(dat, !(hID %in% badhIDs))

    ##the table is multiple to multiple mapping use a list to store
    mouse2humanList = list() 
    for(iHID in sort(unique(dat$hID))) {
        ## print(iHID)
        tmp = filter(dat, hID == iHID)
        mtmp = filter(tmp, taxID == 10090)
        htmp = filter(tmp, taxID == 9606)
        for(ig in as.character(mtmp$Symbol)) {
            ## print(ig)
            mouse2humanList[[length(mouse2humanList)+1]] = htmp$Symbol
            names(mouse2humanList)[length(mouse2humanList)] = ig
        }
    }
    setwd(oldwd)
    mouse2humanList
}
convert = function(input,db) {
    match(input, names(db)) -> idx
    tmp = db[idx]
    for(i in seq_along(tmp)) {
        if(is.null(tmp[[i]])) {tmp[[i]] = NA;next}
        if(length(tmp[[i]]) == 1) next
        tmpi = tmp[[i]]
        if(toupper(input[i]) %in% tmpi) {
            tmp[[i]] = toupper(input[i])
        }else{
            tmp[[i]] = sample(tmpi,1)
        }
    }
    res = unlist(tmp)
    res
}
m2hdb = getDb.m2h()
expr<- as.matrix(GetAssayData(seuratObj))[uniqGenes,start:end]
hGenes = convert(uniqGenes,m2hdb)
hGenes.idx = which(!is.na(hGenes))
print(paste0('# converted genes:',length(hGenes.idx)))
expr = expr[hGenes.idx,,drop = F];rownames(expr) = hGenes[hGenes.idx]

t1  = proc.time()
gsva.res <- gsva(expr, geneSets, annotation,
                      #method=c("gsva"),
     method=c("ssgsea"),
     #kcdf=c("Gaussian"),
     ## rnaseq=T,
     abs.ranking=FALSE,
     min.sz=1,
     max.sz=Inf,
     ## no.bootstraps=0,
     ## bootstrap.percent = .632,
     parallel.sz=ncore,
     parallel.type="SOCK",
     mx.diff=TRUE,
     #tau=switch(method, gsva=1, ssgsea=0.25, NA),
     ## kernel=TRUE,
     ssgsea.norm=TRUE,
     #return.old.value=FALSE,
     verbose=TRUE
     )
t2 = proc.time()
print(t2-t1)
write_tsv(data.frame(rID = rownames(as.data.frame(gsva.res)),
	as.data.frame(gsva.res)), paste0('gsva-res-',start,'-',end,'-',Sys.Date(),'.tsv'))

