####################################################
# rsem+star pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by mirdeep2 and bowtie)
####################################################

if [ $# != 3 ]; then
    echo " usage: sample.fq1 fq2 configFile";
else
    fq1=$1
    fq2=$2
    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R[1|2]*gz/}
    configFile=$3

    output_dir=$(cat $configFile | grep -w 'CUTADAPT_OUTPUT_PATH' | cut -d '=' -f2)
    cutadapt_params=$(cat $configFile | grep -w 'CUTADAPT_PARAMS' | cut -d '=' -f2)

    mkdir -p  $output_dir
    cd $output_dir
    module load python/2.7.6-2-anaconda
    module load cutadapt/1.8.1
    echo "cut adaptors and trim low quality bases"
    cutadapt $cutadapt_params  -o $output_dir/${samplename}_R1.fastq.gz -p $output_dir/${samplename}_R2.fastq.gz $fq1 $fq2> $output_dir/$samplename.cutadapt.log
fi
