####################################################
# co cleaning of bam for one sample(pair)
# inputs: 
#    configFile
####################################################

if [ $# != 4 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
    tbam=
    nbam=
    oname=    #result name
    reference=
    knownIndels=
    captureTarget=
    dbsnpvcf=
    ponvcf=
    cosmicvcf=
    module load gatk/3.5-0 ## tbd
    java -jar GenomeAnalysisTK.jar \
         -T RealignerTargetCreator \
         -R $reference
    -known knownIndels
    -I $tbam
    -I $nbam
    -o $oname.realign_target.intervals

    java -jar GenomeAnalysisTK.jar \
         -T IndelRealigner \
         -R $reference \
         -known $knownIndels \
         -targetIntervals $captureTarget \
         --noOriginalAlignmentTags \
         -I $tbam \
         -I $nbam \
         -nWayOut $oname.map

    java -jar GenomeAnalysisTK.jar \
         -T BaseRecalibrator \
         -R $reference \
         -I $tbam \
         -knownSites $dbsnpvcf
    -o $oname.bqsr.grp

    java -jar GenomeAnalysisTK.jar \
         -T PrintReads \
         -R $reference \
         -I $tbam \
         --BQSR $oname.bqsr.grp \
         -o $oname.recalibed.bam

    java -jar GenomeAnalysisTK.jar \
         -T MuTect2 \
         -R $reference \
         -L $captureTarget \
         -I:$tbam \
         -I:$nbam \
         --normal_panel $ponvcf \                        
    --cosmic $cosmicvcf \
             --dbsnp $dbsnpvcf \
             --contamination_fraction_to_filter 0.02 \                   
    -o $oname.mutect_variants.vcf \
       --output_mode EMIT_VARIANTS_ONLY \
       --disable_auto_index_creation_and_locking_when_reading_rods

    module load  MuSE/v1.0rc

    MuSE call \
         -f <reference> \
         -r <region> \                                   
    <tumor.bam> \
     <normal.bam> \
     -O <intermediate_muse_call.txt>

    MuSE sump \
         -I <intermediate_muse_call.txt> \                           
    -E \                                
    -D <dbsnp_known_snp_sites.vcf> \
       -O <muse_variants.vcf>  
fi
