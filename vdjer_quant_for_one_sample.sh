#!/bin/bash

if [ $# != 2 ]; then
    echo " usage: samdir configFile";
else
    samdir=$1
    configFile=$2
    nthread=$(cat $configFile | grep -w 'VDJER_RSEM_NTHREAD' | cut -d '=' -f2)
    vdjer_samtools_ram=$(cat $configFile | grep -w 'VDJER_SAMTOOLS_RAM' | cut -d '=' -f2)
    cd $samdir
    if [ -s "vdj_contigs.fa" ]; then
        ##we have sample.sam instead of vdjer.sam....
        ln -s $samdir/$(basename $samdir).sam $samdir/vdjer.sam
        ##remember to module load samtools
        samtools view -1 -bS  vdjer.sam > vdjer.bam
        # Sort by name, while keeping read pairs together for input into RSEM
        # Note: you may need to increase the memory passed to sort for larger datasets.
        samtools view -H vdjer.bam > vdjer.namesort.sam
        samtools view vdjer.bam | tr '\t' '~' | paste - - | sort -S $vdjer_samtools_ram | awk '{print $1 "\n" $2}' | tr '~' '\t' >> vdjer.namesort.sam
        samtools view -bS vdjer.namesort.sam > vdjer.namesort.bam

        # RSEM prepare ref
        rsem-prepare-reference  vdj_contigs.fa rsem_vdj

        # RSEM calc expression
        rsem-calculate-expression -p $nthread --paired-end --bam vdjer.namesort.bam rsem_vdj rsem_results
    else
        touch rsem_results.isoforms.results
    fi
fi
