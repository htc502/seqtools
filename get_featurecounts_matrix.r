##remember to module load R/3.3.3-shlib on shark...
if(!require(dplyr)) stop('error loading dplyr')
if(!require(readr)) stop('erro loading readr')
if(!require(rtracklayer)) stop('error loading rtracklayer')

args = commandArgs(trailingOnly = T)
if(length(args)!= 1) stop('usage: Rscript get_featurecounts_matrix.r configFile')
configFile=args[1]
## set before running
featurecounts_root_dir = system(command = paste0("cat ",configFile," | grep -w 'FEATURECOUNTS_OUTPUT_PATH' | cut -d '=' -f2"),intern = T)
output_dir=featurecounts_root_dir
gfffile = system(command = paste0("cat ",configFile," | grep -w 'FEATURECOUNTS_GFF_FILE' | cut -d '=' -f2"),intern = T)
gff = readGFF(gfffile,
              tags = c('gene_id','gene_type','gene_name','transcript_id'))

combineFC <-function(x, sampleID=NA) { ##FC: featurecounts
  root_dir = x
  sub_dirs = list.dirs(root_dir, recursive=F)
  if(is.na(sampleID)) sampleID = basename(sub_dirs)
  readscountFile = paste0(basename(sub_dirs),'.readsCount')
  tmp = read_tsv(file.path(sub_dirs[1],readscountFile[1]),
                   col_names = T,comment= '#')
  GeneName = as.character(tmp$Geneid)
  M = matrix(NA,nrow=length(GeneName),ncol=length(sub_dirs));rm(tmp)
  stat = NA ##deprecated
  geneLen = matrix(NA,nrow=length(GeneName),ncol=length(sub_dirs))
  for(i in seq_along(sub_dirs)) {
    tmp = read_tsv(file.path(sub_dirs[i],readscountFile[i]),
                     col_names = T,
                     comment= '#')
    match(GeneName,tmp$Geneid) -> pos
    tmp = tmp[pos,]
    M[,i] = unlist(tmp[,7])
    geneLen[,i] = unlist(tmp[,6])
  }
  rownames(M) = GeneName; colnames(M) = sampleID
  rownames(geneLen) = GeneName; colnames(geneLen) = sampleID
  return(list(M=M,
              stat=stat,
              geneLen=geneLen))
}

##sample id that wiil be used
##dir name to sample id
sub_dirs = list.dirs(featurecounts_root_dir,recursive=F)
dirs_name = basename(sub_dirs) 
##sample info is not fully populated now
sample_ID = dirs_name


##match(sample_ID, sampleinfo$newname1) -> pos
##sampleinfo = sampleinfo[pos,]
featurecountsObj = combineFC(featurecounts_root_dir,sample_ID)
gff_gene = filter(gff, type == 'gene')
gene_annotation = gff_gene[match(rownames(featurecountsObj$M),gff_gene$gene_id),]
countMtr = tbl_df(data.frame(
  SYMBOL=gene_annotation$gene_name,
  ID=gene_annotation$gene_id,
  Type=gene_annotation$gene_type,
  featurecountsObj$M,
  check.names = F
))
geneLenMtr = tbl_df(data.frame(
   SYMBOL=gene_annotation$gene_name,
  ID=gene_annotation$gene_id,
  Type=gene_annotation$gene_type,
  featurecountsObj$geneLen,
  check.names = F
))
pcg_countMtr = filter(countMtr, Type == 'protein_coding')
nonpcg_countMtr = filter(countMtr, Type != 'protein_coding')
pcg_geneLen = filter(geneLenMtr, Type == 'protein_coding')
nonpcg_geneLen = filter(geneLenMtr, Type != 'protein_coding')
write_tsv(pcg_countMtr, path=file.path(output_dir,'protein_coding_gene_countMatrix.tsv'))
write_tsv(nonpcg_countMtr, path=file.path(output_dir,'Non_protein_coding_gene_countMatrix.tsv'))
write_tsv(pcg_geneLen, path=file.path(output_dir,'protein_coding_gene_geneLengthMatrix.tsv'))
write_tsv(nonpcg_countMtr, path=file.path(output_dir,'Non_protein_coding_gene_geneLengthMatrix.tsv'))

##calculate FPKM and FPKM-uq(https://docs.gdc.cancer.gov/Data/Bioinformatics_Pipelines/Expression_mRNA_Pipeline/#data-processing-steps)
myfpkm = function(RCg,RCpc,L) RCg*10e9/(RCpc*L)
myfpkmuq = function(RCg,RCg75,L) RCg*10e9/(RCg75*L)

##RCg: Number of reads mapped to the gene
##RCpc: Number of reads mapped to all protein-coding genes
##RCg75: The 75th percentile read count value for genes in the sample
##L: Length of the gene in base pairs; Calculated as the sum of all exons in a gene
countMtr = select(pcg_countMtr, -SYMBOL,
                  -ID,-Type)
geneLen = unlist(pcg_geneLen[,4]) ##L
libSz = apply(countMtr, 2, sum) ##RCpc 
fpkm = t(apply(countMtr*1e9,1,function(rw,libSz) rw/libSz,
             libSz = libSz))
fpkm = apply(fpkm, 2, function(cl,geneLen) cl/geneLen,
             geneLen = geneLen)
fpkm = tbl_df(data.frame(
  select(pcg_countMtr, SYMBOL, ID, Type),
  fpkm,
  check.names =F
))
write_tsv(fpkm, path=file.path(output_dir,'protein_coding_gene_fpkm.tsv'))

##fpkm with edger
library(edgeR)
fpkm.edger = rpkm(countMtr, gene.length = geneLen)
fpkm.edger = tbl_df(data.frame(
  select(pcg_countMtr, SYMBOL, ID, Type),
  fpkm.edger,
  check.names = F
))
##produce the same result...
write_tsv(fpkm.edger, path=file.path(output_dir,'protein_coding_gene_fpkm_edger.tsv'))
##want a correlation heatmap plot?
gene.df = select(pcg_countMtr, SYMBOL,ID)
dat.df = select(pcg_countMtr, -SYMBOL, -ID,-Type) 
##remove zero
low.idx = rowSums(dat.df) == 0
dat.df = dat.df[!low.idx,]
gene.df = gene.df[!low.idx,]
corrSamples = cor(dat.df,method = 'pearson')
library(pheatmap)
library(RColorBrewer)
pheatmap(mat=corrSamples,
         ##          color = colorRampPalette((brewer.pal(n = 7, name = "RdYlBu")))(100),
         ##         cluster_cols=F,
         ##         show_colnames = F,
         border_color = 'white',
         show_colnames=F,
         filename=file.path(output_dir,'sampleCorrelation.readcount.heatmap.pdf'))

