####################################################
# featurecounts pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: sampleName configFile";
else
    samplefilepath=$1
    configFile=$2

	################featurecounts###############################
  output_dir=$(cat $configFile | grep -w 'FEATURECOUNTS_OUTPUT_PATH' | cut -d '=' -f2)
  gff_file=$(cat $configFile | grep -w 'FEATURECOUNTS_GFF_FILE' | cut -d '=' -f2)
  strand=$(cat $configFile | grep -w 'FEATURECOUNTS_STRAND' | cut -d '=' -f2)
  nthreads=$(cat $configFile | grep -w 'FEATURECOUNTS_NTHREADS' | cut -d '=' -f2)


  bamfile=$samplefilepath
  samplename=`basename $bamfile`
  samplename=${samplename/.bam/}
	
  output_dir1=${output_dir}/${samplename}

	mkdir -p $output_dir1
	cd $output_dir1

	echo "run featurecounts on data ..."
  echo "featureCounts -p -s $strand -T $nthreads -a $gff_file -t exon -g gene id  -o $output_dir1/$samplename.readsCount $bamfile 2> $output_dir1/${samplename}.featurecounts.error.log"
  featureCounts -p -s $strand -T $nthreads -a $gff_file -t exon -g gene_id  -o $output_dir1/$samplename.readsCount $bamfile 2> $output_dir1/${samplename}.featurecounts.error.log 1>$output_dir1/${samplename}.featurecounts.stdout.log
fi
