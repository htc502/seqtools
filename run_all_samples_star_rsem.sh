####################################################
# rsem+star pipeline: fastQC for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create rsem_star_mapping_sample_scripts subdir..."
    rsem_star_mapping_sample_scripts_path=${SCRIPTS_PATH}/rsem_star_mapping_sample_scripts
    mkdir -p $rsem_star_mapping_sample_scripts_path

    ##rsem_star will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=`ls ${CUTADAPT_OUTPUT_PATH}/*.cutadapt.fq`

    echo ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        while [ " " == " " ]
        do
            d=`ps -aux | grep 'rsem-calculate-expression' | wc -l`
            #echo "line=$line"
            FASTQ=${line/.cutadapt.fq//}
            if [ $d -le 2 ]
            then
                echo "$c=$line"
                echo "-------------------------------"
                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh {SCRIPTS_PATH}/star_for_one_sample.sh ${FASTQ} ${SCRIPTS_PATH}/config/tool_info.txt" >  ${rsem_star_mapping_sample_scripts_path}/rsem_star_${FASTQ}.sh
                chmod 755  ${rsem_star_mapping_sample_scripts_path}/rsem_star_${FASTQ}.sh
                sh  ${rsem_star_mapping_sample_scripts_path}/rsem_star_${FASTQ}.sh &
                sleep 2
                break
            else
                sleep 30
            fi
        done
        echo "end"
    done

fi
