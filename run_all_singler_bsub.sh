## 19-02-22 11:26:50
## (What it does) singler data prepare
## (to manuscript, method or conclusion)
## (To save life, please try to finish within 150 lines)

#!/bin/sh
if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    JOB_LSF_PATH=$SCRIPTS_PATH/singleR_job_lsf
mkdir -p $JOB_LSF_PATH
    INPUT_PATH=$(cat $configFile | grep -w 'SINGLER_SEURATOBJ_PATH' | cut -d '=' -f2)
    NCELL_INPUT=$(cat $configFile | grep -w 'SINGLER_SEURATOBJ_NCELL' | cut -d '=' -f2)
    OUTPUT_PATH=$(cat $configFile | grep -w 'SINGLER_OUTPUT_PATH' | cut -d '=' -f2)
echo $JOB_LSF_PATH
echo $OUTPUT_PATH
echo $INPUT_PATH
echo $NCELL_INPUT

mkdir -p $OUTPUT_PATH
echo "looping through each part of cells ..."

for i in $(seq 1 2000 $NCELL_INPUT); do
    echo "$i"
istart=$i
iend=$(($istart+2000))
iend=$(($iend-1))
if [ $iend -gt $NCELL_INPUT ];then
iend=$NCELL_INPUT
fi
echo "$istart-$iend"
    echo "#BSUB -J sr_$i
#BSUB -W 1:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/sr_${i}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/sr_${i}.e 
#BSUB -q short
#BSUB -n  10
#BSUB -M 51200
#BSUB -R rusage[mem=51200]
#BSUB -B
#BSUB -N

module load python3/3.6.5-2-anaconda
module load R/3.5.2-shlib

Rscript $SCRIPTS_PATH/singler-for-one-batch.r $INPUT_PATH 10 $istart $iend $OUTPUT_PATH " > ${JOB_LSF_PATH}/sr_${i}.lsf
    bsub< ${JOB_LSF_PATH}/sr_${i}.lsf
    sleep 5
done
fi
