####################################################
# vdjer_quant pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create vdjer_quant_sample_scripts subdir..."
    vdjer_quant_sample_scripts_path=${SCRIPTS_PATH}/vdjer_quant_sample_scripts
    mkdir -p $vdjer_quant_sample_scripts_path

    SAM_DIR_LIST=$(cat $configFile | grep -w 'VDJER_SAM_DIR_LIST_FILE' | cut -d '=' -f2)

    cat ${SAM_DIR_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        samdir=$line
        samplename=`basename $samdir`

        echo "#BSUB -J MMvdjer_quant_${samplename}
#BSUB -W 3:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/miles_MMvdjer_quant_${samplename}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/miles_MMvdjer_quant_${samplename}.e 
#BSUB -cwd /rsrch2/genomic_med/ghan1/ 
#BSUB -q medium
#BSUB -n  13
#BSUB -M 65536
#BSUB -R rusage[mem=65536]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

echo "submitting vdjer_quant_${samplename} jobs"
module load samtools/1.4
module load rsem/1.2.31

sh ${SCRIPTS_PATH}/vdjer_quant_for_one_sample.sh $line ${SCRIPTS_PATH}/config/tool_info.txt " > ${vdjer_quant_sample_scripts_path}/vdjer_quant_${samplename}.lsf
        bsub< ${vdjer_quant_sample_scripts_path}/vdjer_quant_${samplename}.lsf
        sleep 3

    done

fi
