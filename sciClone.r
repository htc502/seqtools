rm(list = ls())
args = commandArgs(trailingOnly = T)
if(!(length(args)== 1) ) stop('usage: Rscript sciClone.r configFile')
configFile=args[1]
sampleInfo = system(command = paste0("cat ",configFile," | grep -w 'GATKVCF_REFORM_TABLE_FILE' | cut -d '=' -f2"),intern = T)
sequenzaPlotDir=system(command = paste0("cat ",configFile," | grep -w 'SEQUENZA_PLOTS_DIR' | cut -d '=' -f2"),intern = T)
cnDir = system(command = paste0("cat ",configFile," | grep -w 'SCICLONE_CNV_INPUT' | cut -d '=' -f2"),intern = T)
sciCloneDir=system(command = paste0("cat ",configFile," | grep -w 'SCICLONE_OUTPUT' | cut -d '=' -f2"),intern = T)
mutectDir=system(command = paste0("cat ",configFile," | grep -w 'SCICLONE_MUTATION_INPUT' | cut -d '=' -f2"),intern = T)
vafs<-NULL;
copyNumberCalls<-NULL;
regionsToExclude<-NULL;
library(readr)
reformTable = read_tsv(sampleInfo,
                       col_names = F)
library(sciClone)
for (i in 1:nrow(reformTable)){
  sample = reformTable[i,]$X3
  samplename=basename(sample);samplename = gsub('.unif.snp.txt','',samplename)
  print(c(i,samplename))

  LOHAlg = 'sequenza'
####### sequenza
  if(LOHAlg == 'sequenza') {
    data<-read.table(paste(sequenzaPlotDir,'/',samplename,'/seg.tab.',samplename,'.txt',sep=''),header = T,sep = '\t')
    LOH<-data[(data$A==0 & data$B!=0) | (data$A!=0 & data$B==0),];
    LOH<-LOH[,c('chromosome','start.pos','end.pos')]
    print(dim(LOH))
    regionsToExclude[[i]]<-LOH;
  }

  vaf<-read.delim(file.path(mutectDir,paste0(samplename,'.tsv')),sep='\t',header = T);
  vaf.u<-vaf[,c('chr','start','t_ref_count','t_alt_count','tumor_f')];
  vaf.u$tumor_f<-vaf.u$tumor_f*100;
  print(dim(vaf.u))
  vafs[[i]]<-vaf.u;

  cn<-read.delim(file.path(cnDir,paste0(samplename,'.tsv')),header = T,sep='\t');
  cn<-cn[,c('chrom','loc.start','loc.end','seg.mean')]
  print(dim(cn))
  copyNumberCalls[[i]]<-cn;

  sc = tryCatch(sciClone(vafs=vaf.u,
                copyNumberCalls=cn,
                sampleNames=samplename,
                regionsToExclude=LOH,
                cnCallsAreLog2=T,
                minimumDepth=20),
error = function(cond) {return(NULL)}
)
if(!is.null(sc)) {
  writeClusterTable(sc, paste(sciCloneDir,'/',samplename,'.clusters1.',LOHAlg,'.pdf',sep = ''))
  sc.plot1d(sc,paste(sciCloneDir,'/',samplename,'.clusters1.1d.',LOHAlg,'.pdf',sep = ''))
}
}
