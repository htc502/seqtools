######### R #########
rm(list = ls())
args = commandArgs(trailingOnly = T)
if(!(length(args)== 1) ) stop('usage: Rscript sequenza_downstream.r configFile')

library(copynumber)
library(sequenza)
configFile = args[1]
gatk4ascatdir=system(command = paste0("cat ",configFile," | grep -w 'GATK4ASCAT_OUTPUT_DIR' | cut -d '=' -f2"),intern = T)
sequenzadir=system(command = paste0("cat ",configFile," | grep -w 'SEQUENZA_OUTPUT_DIR' | cut -d '=' -f2"),intern = T)
sequenzaPlot=system(command = paste0("cat ",configFile," | grep -w 'SEQUENZA_PLOTS_DIR' | cut -d '=' -f2"),intern = T)
print(gatk4ascatdir)
file<-list.files(gatk4ascatdir,pattern = '.*.unif.snp.txt')
for(i in 1:length(file)){
  sample = gsub('.unif.snp.txt','',file[i]);
  print(paste0(i,":",sample))
  data.file = paste(sequenzadir,'/',sample,"_small.seqz.gz",sep="") ;
  seqz.ext = sequenza.extract(data.file);
  ## Inference of cellularity and ploidy
  CP = sequenza.fit(seqz.ext) 
  cint = get.ci(CP);
    
  ## Call CNVs and mutations using the estimated parameters
  cellularity = cint$max.cellularity
  ploidy = cint$max.ploidy
  avg.depth.ratio = mean(seqz.ext$gc$adj[,2])
  
  print(paste(cellularity, ploidy, avg.depth.ratio, sep="--"));
  dir.create(paste(sequenzaPlot,'/',sample,sep=""));
  
  ## Detect variant alleles (mutations)
  mut.tab = na.exclude(do.call(rbind, seqz.ext$mutations))
  mut.alleles = mufreq.bayes(mufreq = mut.tab$F,
                             depth.ratio = mut.tab$adjusted.ratio,
                             cellularity = cellularity, ploidy = ploidy,
                             avg.depth.ratio = avg.depth.ratio)
  
  mut.alleles.bind = cbind(mut.tab[,c("chromosome","position","F","adjusted.ratio", "mutation")], mut.alleles)
  write.table(mut.alleles.bind, file=paste(sequenzaPlot,'/',sample,"/mut.alleles.",sample,".txt", sep=""),row.names=FALSE,sep="\t", quote=FALSE)
  
  ## Detect Segment, important information above for pyclone
  seg.tab = na.exclude(do.call(rbind, seqz.ext$segments))
  cn.alleles = baf.bayes(Bf = seg.tab$Bf, depth.ratio = seg.tab$depth.ratio,
                         cellularity = cellularity, ploidy = ploidy,
                         avg.depth.ratio = avg.depth.ratio)
  
  seg.tab.bind = cbind(seg.tab, cn.alleles)
  write.table(seg.tab.bind, file=paste(sequenzaPlot,'/',sample,"/seg.tab.",sample,".txt", sep=""),row.names=FALSE,sep="\t", quote=FALSE)
  ## Plot and sequenza results
  sequenza.results(sequenza.extract = seqz.ext, cp.table = CP, sample.id = sample, out.dir=paste(sequenzaPlot,'/',sample,sep=""))
  
  ## CP
  cp.plot1 = function (cp.table, xlab = "Ploidy", ylab = "Cellularity", zlab = "Scaled rank LPP",main =sample,
                       colFn = colorRampPalette(c("white", "green3")), ...) {
    z <- matrix(rank(cp.table$lpp), nrow = nrow(cp.table$lpp))/length(cp.table$lpp)
    map <- makecmap(c(0, 1), colFn = colFn, include.lowest = TRUE)
    colorgram(x = cp.table$ploidy, y = cp.table$cellularity, z = z, map = map, las = 1, xlab = xlab, ylab = ylab, main=main, zlab = zlab, ...)
    box(col="gray")
  }
  pdf(paste(sequenzaPlot,'/',sample,"/CP.",sample,".pdf",sep=""), height=5, width=5)
  cp.plot1(CP,main = sample)
  cp.plot.contours(CP, add = TRUE,col="deeppink")
  dev.off();
  ## GC content
  seqz.data = read.seqz(data.file)
  gc.stats = gc.norm(x = seqz.data$depth.ratio, gc = seqz.data$GC.percent);
  ## gc.stats1 = gc.sample.stats(data.file)  ## same function as above
  
  ## normalization of depth ratio
  gc.vect = setNames(gc.stats$raw.mean, gc.stats$gc.values)
  seqz.data$adjusted.ratio = seqz.data$depth.ratio / gc.vect[as.character(seqz.data$GC.percent)]
  
  pdf(paste(sequenzaPlot,'/',sample,"/GCsequenza.",sample,".pdf",sep=""), height=5, width=10)
  par(mfrow = c(1,2), cex = 1, las = 1, bty = 'l')
  matplot(gc.stats$gc.values, gc.stats$raw,
          type = 'b', col = 1, pch = c(1, 19, 1), lty = c(2, 1, 2),
          xlab = 'GC content (%)', ylab = 'Uncorrected depth ratio')
  legend('topright', legend = colnames(gc.stats$raw), pch = c(1, 19, 1))
  hist2(seqz.data$depth.ratio, seqz.data$adjusted.ratio,
        breaks = prettyLog, key = vkey, panel.first = abline(0, 1, lty = 2),
        xlab = 'Uncorrected depth ratio', ylab = 'GC-adjusted depth ratio',main=sample)
  dev.off()
  
  ## Plot chromosome view with mutations, BAF, depth ratio and segments
  pdf(paste(sequenzaPlot,'/',sample,"/Mut.sequenza.",sample,".pdf",sep=""), height=8, width=10)
  chromosome.view(mut.tab = seqz.ext$mutations[[1]], baf.windows = seqz.ext$BAF[[1]],
                  ratio.windows = seqz.ext$ratio[[1]], min.N.ratio = 1,
                  segments = seqz.ext$segments[[1]], main = seqz.ext$chromosomes[1])
  dev.off()
  
  ## Visualize detected copy number changes and variant alleles
  pdf(paste(sequenzaPlot,'/',sample,"/CNV.sequenza.",sample,".pdf",sep=""), height=10, width=10)
  chromosome.view(mut.tab = seqz.ext$mutations[[3]], baf.windows = seqz.ext$BAF[[3]],
                  ratio.windows = seqz.ext$ratio[[3]], min.N.ratio = 1,
                  segments = seg.tab.bind[seg.tab.bind$chromosome == seqz.ext$chromosomes[3],],
                  main = seqz.ext$chromosomes[3],
                  cellularity = cellularity, ploidy = ploidy,
                  avg.depth.ratio = avg.depth.ratio)
  dev.off()
  
  ## Genome-wide absolute copy number profile obtained from exome sequencing.
  pdf(paste(sequenzaPlot,'/',sample,"/genome.sequenza.",sample,".pdf",sep=""), height=4, width=10)
  genome.view(seg.cn = seg.tab.bind, info.type = "CNt")
  legend("bottomright", bty="n", c("Tumor copy number"),col = c("red"),inset = c(0, -0.4), pch=15, xpd = TRUE)
  dev.off()
  
  pdf(paste(sequenzaPlot,'/',sample,"/genome1.sequenza.",sample,".pdf",sep=""), height=4, width=10)
  genome.view(seg.cn = seg.tab.bind, info.type = "AB")
  legend("bottomright", bty = "n", c("A-allele","B-allele"), col= c("red", "blue"),inset = c(0, -0.45), pch = 15, xpd = TRUE)
  dev.off()
  
  save.image(file=paste(sequenzaPlot,'/',sample,"/",sample,".extract.RData",sep=""))
}

