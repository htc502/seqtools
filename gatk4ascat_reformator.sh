####################################################
# gatk vcf reformator for ascat: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create gatkvcf_reform_sample_scripts subdir..."
    gatkvcf_reform_sample_scripts_path=${SCRIPTS_PATH}/gatkvcf_reform_sample_scripts
    mkdir -p $gatkvcf_reform_sample_scripts_path

    REFORMATOR_TABLE=$(cat $configFile | grep -w 'GATKVCF_REFORM_TABLE_FILE' | cut -d '=' -f2)

    cat ${REFORMATOR_TABLE} | while read line; do
        fields=( $line )
        tumorSampleID=${fields[0]}
        infile=${fields[1]}
        outfile=${fields[2]}
        sampleID=`basename $infile`
	echo "$sampleID"
        echo "#BSUB -J ${sampleID}_gatkvcf_reform
#BSUB -W 1:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${sampleID}_gatkvcf_reform.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${sampleID}_gatkvcf_reform.e 
#BSUB -q short
#BSUB -n 1
#BSUB -M 61440
#BSUB -R rusage[mem=61440]
#BSUB -B
#BSUB -N

module load python/2.7.9-2-anaconda
python ${SCRIPTS_PATH}/gatkVCF_reform_one_sample.py $infile $outfile $tumorSampleID ${SCRIPTS_PATH}/config/tool_info.txt" >  ${gatkvcf_reform_sample_scripts_path}/gatkvcf_reform_${sampleID}.lsf
        bsub <  ${gatkvcf_reform_sample_scripts_path}/gatkvcf_reform_${sampleID}.lsf 
        sleep 1
    done

fi
