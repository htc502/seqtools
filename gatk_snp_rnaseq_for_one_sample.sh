####################################################
# gatk rnaseq vairant calling pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
echo " usage: sampleName configFile";
else
sample=$1
configFile=$2
JAVA_BIN=$(cat $configFile | grep -w 'GATK_JAVA_BIN' | cut -d '=' -f2)
java_mem=$(cat $configFile | grep -w 'GATK_JAVA_MEM' | cut -d '=' -f2)
GATK_jar=$(cat $configFile | grep -w 'GATK_JAR' | cut -d '=' -f2)
reference_file=$(cat $configFile | grep -w 'GATK_REF_GENOME' | cut -d '=' -f2)
input_dir=$(cat $configFile | grep -w 'GATK_BAM_INPUT_PATH' | cut -d '=' -f2)
output_dir=$(cat $configFile | grep -w 'GATK_OUTPUT_PATH' | cut -d '=' -f2)
GATK_KNOW_SNP_VCF=$(cat $configFile | grep -w 'GATK_KNOW_SNP_VCF' | cut -d '=' -f2)

bamfile=${sample}.bam

cd ${output_dir}
echo $java_mem
echo "run gatk snp calling(RNAseq) on ${bamfile}"
##Add read groups, sort, mark duplicates, and create index
##java -jar picard.jar AddOrReplaceReadGroups I=star_output.sam O=rg_added_sorted.bam SO=coordinate RGID=id RGLB=library RGPL=platform RGPU=machine RGSM=sample 
##java -jar picard.jar MarkDuplicates I=rg_added_sorted.bam O=dedupped.bam  CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT M=output.metrics 

##Split'N'Trim and reassign mapping qualities
##${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T SplitNCigarReads -R ${reference_file} -I ${input_dir}/${bamfile} -o  ${output_dir}/${sample}.split.bam -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS

##indel realignment ignored

##BQSR
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T BaseRecalibrator -I ${output_dir}/${sample}.split.bam -R ${reference_file} --knownSites $GATK_KNOW_SNP_VCF  -o ${output_dir}/${sample}.recalibration_report.grp

##Base Recalibration
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T PrintReads -R ${reference_file} -I ${output_dir}/${sample}.split.bam -BQSR ${output_dir}/${sample}.recalibration_report.grp -o   ${output_dir}/${sample}.BQSR.bam

##variant calling
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar} -T HaplotypeCaller -R ${reference_file} -I ${output_dir}/${sample}.BQSR.bam -dontUseSoftClippedBases -stand_call_conf 20.0 -o ${output_dir}/${sample}.vcf

##hard filtering
${JAVA_BIN} -Xmx${java_mem} -jar ${GATK_jar}  -T VariantFiltration  -R ${reference_file} -V ${output_dir}/${sample}.vcf -window 35 -cluster 3 -filterName FS -filter "FS > 30.0" -filterName QD -filter "QD < 2.0" -o ${output_dir}/${sample}.filterLabelled.vcf 
fi
