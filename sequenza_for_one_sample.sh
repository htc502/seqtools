####################################################
# this is sequenza for one sample
#  inputs:
#     configFile (for reference, capture bed file, outputdir)
#     tumorbam
#     normalbam
#     ID (for output file name)
####################################################

if [ $# != 4 ]; then
    echo " usage: tumor.Bam normal.Bam outputSampleID configFile";
else
    tbam=$1
    nbam=$2
    sampleID=$3
    configFile=$4

    genomeRef=$(cat $configFile | grep -w 'SEQUENZA_GENOME_REF_FILE' | cut -d '=' -f2)
    captureBed=$(cat $configFile | grep -w 'SEQUENZA_CAPTURE_BED_FILE' | cut -d '=' -f2)
    outputDir=$(cat $configFile | grep -w 'SEQUENZA_OUTPUT_DIR' | cut -d '=' -f2)
    gc50=$(cat $configFile | grep -w 'GC50' | cut -d '=' -f2)

    module load samtools/1.4
    module load R/3.1.0-shlib
    module load python/2.7.9-2-anaconda

##    samtools mpileup -l $captureBed \
##             -f $genomeRef \
##             -Q 20 \
##             $tbam | gzip > $outputDir/${sampleID}_T.pileup.gz
##    
##   samtools mpileup -l $captureBed \
##            -f $genomeRef \
##            -Q 20 \
##            $nbam | gzip > $outputDir/${sampleID}_N.pileup.gz
##
   sequenza-utils bam2seqz --pileup  -n $outputDir/${sampleID}_N.pileup.gz -t $outputDir/${sampleID}_T.pileup.gz -gc $gc50 -o $outputDir/$sampleID.seqz.gz

   sequenza-utils seqz_binning -w 50 -s $outputDir/$sampleID.seqz.gz  -o $outputDir/${sampleID}_small.seqz.gz
   ##python /rsrch2/genomic_med/ghan1/R/x86_64-unknown-linux-gnu-library/3.1/sequenza/exec/sequenza-utils.py pileup2seqz -gc $gc50 -n $outputDir/${sampleID}_N.pileup.gz -t $outputDir/${sampleID}_T.pileup.gz | gzip > $outputDir/$sampleID.seqz.gz
    ##python /rsrch2/genomic_med/ghan1/R/x86_64-unknown-linux-gnu-library/3.1/sequenza/exec/sequenza-utils.py seqz-binning -w 50 -s $outputDir/$sampleID.seqz.gz | gzip > $outputDir/${sampleID}_small.seqz.gz

fi
