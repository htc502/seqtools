use File::Basename;
##use Getopt::Std;
##getopt("a", \%opts);
##$sampleName = $opts{'a'};
##getopt("b", \%opts);
##$sampleFileDIR= $opts{'b'};
##getopt("c", \%opts);
##$outputDIR= $opts{'c'};
##$sampleFileDIR = $ARGV[1];
$fqR1 = $ARGV[1];
$fqR2 = $ARGV[2];
$outputDIR = $ARGV[3];

mkdir($outputDIR, 0700)  unless(-d $outputFile );
%badReadHash=();
@filenames = ($fqR1, $fqR2);
foreach(@filenames){
	chomp;
	$file = $_;
  if ($file =~ /.gz$/) {
      open($fileData, "gunzip -c $file |") || die "can’t open pipe to $file";
  } else {
      open($fileData, $file) || die "can’t open $file";
  }
	while (! eof $fileData) {
		chomp;
		$line=$_;
		my $line1 = <$fileData>;
		my $readID = (split(/ /,$line1))[0];
		my $line2 = <$fileData>;
		my $line3 = <$fileData>;
		my $ascvalue = <$fileData>;
		@ascii = (split(//,$ascvalue));
		$badCounts = 0;
		for my $asc (@ascii) {
			$ibm = unpack("C*", $asc);
			$score = $ibm-33;
			if ( $score < 20) {
				$badCounts =  $badCounts +  1;
			}
		}
		if ( $badCounts > 10) {
			if ( exists($badReadHash{$readID}) ) {
				$badReadHash{$readID} = $badReadHash{$readID} + 1
			} else {
				$badReadHash{$readID} = 1;     
			}
		}
	}
}

$numBadRead = scalar keys %badReadHash;
print "Fnished Hash\t";
print "$numBadRead\n";
foreach(@filenames){
	chomp;
  $file = $_;
  $fname = basename($file);
	$outputFile = $outputDIR."/".$fname;
	open(OUT, "| gzip > $outputFile");
  if ($file =~ /.gz$/) {
      open($fileData2, "gunzip -c $file |") || die "can’t open pipe to $file";
  } else {
      open($fileData2, $file) || die "can’t open $file";
  }
	while (! eof $fileData2) {
		chomp;
		$line=$_;
		my $line12 = <$fileData2>;
		my $readID = (split(/ /,$line12))[0];
		my $line22 = <$fileData2>;
		my $line32 = <$fileData2>;
		my $line42 = <$fileData2>;
		if ( exists($badReadHash{$readID}) ) {
		} else {
			print OUT "$line12";
			print OUT "$line22";
			print OUT "$line32";
			print OUT "$line42";
		}
	}
}
