####################################################
# star2trust pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create star4trust_mapping_sample_scripts subdir..."
    star4trust_mapping_sample_scripts_path=${SCRIPTS_PATH}/star4trust_mapping_sample_scripts
    mkdir -p $star4trust_mapping_sample_scripts_path

    ##star4trust will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'STAR4TRUST_SAMPLE_LIST_FILE' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
        fields=( $line )
        fq1=${fields[0]}
        fq2=${fields[1]}
        samplename=`basename $fq1`
        samplename=${samplename/[.|_]R1*gz/}
        echo "#BSUB -J ${samplename}_star4trust
#BSUB -W 12:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${samplename}_star4trust.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${samplename}_star4trust.e 
#BSUB -cwd /rsrch2/genomic_med/ghan1/PD1_inhibitor_miles_analysis_remaining2samples
#BSUB -q medium
#BSUB -n  14
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

module load star/2.5.2b
module load jdk/1.8.0_45
module load picard/2.9.0

sh ${SCRIPTS_PATH}/star_for_trust_for_one_sample.sh $fq1 $fq2 ${SCRIPTS_PATH}/config/tool_info.txt" >  ${star4trust_mapping_sample_scripts_path}/star4trust_${samplename}.lsf
##        bsub <  ${star4trust_mapping_sample_scripts_path}/star4trust_${samplename}.lsf 
        sleep 2
    done

fi
