####################################################
# star2pass pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create star2pass_mapping_sample_scripts subdir..."
    star2pass_mapping_sample_scripts_path=${SCRIPTS_PATH}/star2pass_mapping_sample_scripts
    mkdir -p $star2pass_mapping_sample_scripts_path

    ##star2pass will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'STAR2PASS_SAMPLE_LIST_FILE' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        c=0
        echo "$line"
        let c=c+1
fields=( $line )
fq1=${fields[0]}
fq2=${fields[1]}
samplename=`basename $fq1`
samplename=${samplename/[.|_]R1*gz/}
njob=3
        while [ " " == " " ]
        do
            d=`ps -aux | grep 'star2pass_for_one_sample' | wc -l`
            #echo "line=$line"
            if [ $d -le $njob ]
            then
                echo "$c=$line"
                echo "-------------------------------"
                echo "#!/bin/sh
#$ -cwd
#$ -S /bin/sh

sh ${SCRIPTS_PATH}/star2pass_for_one_sample.sh $fq1 $fq2 ${SCRIPTS_PATH}/config/tool_info.txt" >  ${star2pass_mapping_sample_scripts_path}/star2pass_${samplename}.sh
                chmod 755  ${star2pass_mapping_sample_scripts_path}/star2pass_${samplename}.sh
                sh  ${star2pass_mapping_sample_scripts_path}/star2pass_${samplename}.sh &
                sleep 2
                break
            else
                sleep 30
            fi
        done
        echo "end"
    done

fi
