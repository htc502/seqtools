####################################################
# bwa mem (DNAseq) pipeline: for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create bwamem_mapping_sample_scripts subdir..."
    bwamem_mapping_sample_scripts_path=${SCRIPTS_PATH}/bwamem_mapping_sample_scripts
    mkdir -p $bwamem_mapping_sample_scripts_path

    ##bwamem will only do QC on those samples which undergo cutadapt...
    FASTQ_LIST=$(cat $configFile | grep -w 'BWAMEM_SAMPLE_LIST_FILE' | cut -d '=' -f2)

    cat ${FASTQ_LIST} | while read line; do
        echo "$line"
        fields=( $line )
        fq1=${fields[0]}
        fq2=${fields[1]}
        rg=${fields[2]}
        samplename=`basename $fq1`
        samplename=${samplename/[.|_]R1*gz/}
        echo "#BSUB -J ${samplename}_bwamem
#BSUB -W 6:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/${samplename}_bwamem.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/${samplename}_bwamem.e 
#BSUB -q medium
#BSUB -n  14
#BSUB -M 102400
#BSUB -R rusage[mem=102400]
#BSUB -B
#BSUB -N

sh ${SCRIPTS_PATH}/bwa_mem_for_one_sample.sh $fq1 $fq2 $rg ${SCRIPTS_PATH}/config/tool_info.txt" >  ${bwamem_mapping_sample_scripts_path}/bwamem_${samplename}.lsf
        bsub <  ${bwamem_mapping_sample_scripts_path}/bwamem_${samplename}.lsf 
        sleep 2
    done


fi
