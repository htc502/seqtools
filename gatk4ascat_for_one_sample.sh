####################################################
# this is gatk unifiedgenotyper for ascat usage for one sample
#  inputs:
#     configFile (for reference, capture bed file, outputdir)
#     tumorbam
#     normalbam
#     ID (for output file name)
####################################################

if [ $# != 4 ]; then
    echo " usage: tumor.Bam normal.Bam outputSampleID configFile";
else
    tbam=$1
    nbam=$2
    sampleID=$3
    configFile=$4

    genomeRef=$(cat $configFile | grep -w 'GATK4ASCAT_GENOME_REF_FILE' | cut -d '=' -f2)
    captureBed=$(cat $configFile | grep -w 'GATK4ASCAT_CAPTURE_BED_FILE' | cut -d '=' -f2)
    outputDir=$(cat $configFile | grep -w 'GATK4ASCAT_OUTPUT_DIR' | cut -d '=' -f2)
    JAR_GATK4ASCAT=$(cat $configFile | grep -w 'GATK4ASCAT_JAR' | cut -d '=' -f2)
    ##load module in run all sample bsub file
    java -jar $JAR_GATK4ASCAT \
     -T UnifiedGenotyper \
     -R $genomeRef \
     -I $tbam \
     -I $nbam \
     -nt 10 \
     -o ${outputDir}/${sampleID}.unif.snp.vcf \
     -stand_call_conf 50 \
     -L $captureBed
fi
