## 18-12-27 11:12:47
## (What it does) gsva script
## (to manuscript, method or conclusion)
## (To save life, please try to finish within 150 lines)
rm(list = ls());library(readr)
args = commandArgs(trailingOnly=TRUE)
if(length(args) != 6) stop('error start and stop parameters')
start = args[1];stop = args[2];ncore=args[3]
# start = args['start'];stop = args['stop']
## start = 1;stop = 50
outputD=args[4];
if(!dir.exists(outputD)) dir.create(outputD)
setwd(outputD)
library(GSVA)
library('GSEABase');
library('cogena')
gmtFile=args[5]
objFile=args[6]
print(paste0(start,stop, ncore, outputD,gmtFile,objFile))
geneSets <- gmt2list(gmtFile);
library(Seurat)
seuratObj=readRDS(objFile)

expr<- GetAssayData(seuratObj)
expr = expr[,start:stop,drop = F]
expr<- as.matrix(expr)
t1  = proc.time()
gsva.res <- gsva(expr, geneSets, annotation,
                      #method=c("gsva"),
     method=c("ssgsea"),
     #kcdf=c("Gaussian"),
     ## rnaseq=T,
     abs.ranking=FALSE,
     min.sz=1,
     max.sz=Inf,
     ## no.bootstraps=0,
     ## bootstrap.percent = .632,
     parallel.sz=ncore,
     parallel.type="SOCK",
     mx.diff=TRUE,
     #tau=switch(method, gsva=1, ssgsea=0.25, NA),
     ## kernel=TRUE,
     ssgsea.norm=TRUE,
     #return.old.value=FALSE,
     verbose=TRUE
     )
t2 = proc.time()
print(t2-t1)
write_tsv(data.frame(rID = rownames(as.data.frame(gsva.res)),
	as.data.frame(gsva.res)), paste0('gsva-res-',start,'-',stop,'-',Sys.Date(),'.tsv'))

