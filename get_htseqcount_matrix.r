##remember to module load R/3.3.3-shlib on shark...
if(!require(dplyr)) stop('error loading dplyr')
if(!require(readr)) stop('erro loading readr')
if(!require(rtracklayer)) stop('error loading rtracklayer')

args = commandArgs(trailingOnly = T)
if(length(args)!= 1) stop('usage: Rscript get_htseqcount_matrix.r configFile')
configFile=args[1]
## set before running
htseq_root_dir = system(command = paste0("cat ",configFile," | grep -w 'HTSEQCOUNT_OUTPUT_PATH' | cut -d '=' -f2"),intern = T)
output_dir=htseq_root_dir
gfffile = system(command = paste0("cat ",configFile," | grep -w 'HTSEQCOUNT_GFF_FILE' | cut -d '=' -f2"),intern = T)
gff = readGFF(gfffile,
              tags = c('gene_id','gene_type','gene_name','transcript_id'))

combineHTseq <-function(x, sampleID=NA) {
  root_dir = x
  sub_dirs = list.dirs(root_dir, recursive=F)
  if(is.na(sampleID)) sampleID = basename(sub_dirs)
  readscountFile = paste0(basename(sub_dirs),'.readsCount')
  tmp = read.delim(file.path(sub_dirs[1],readscountFile[1]),header=F)
  tmp_stat = tmp[(nrow(tmp)-4):nrow(tmp),]
  tmp = tmp[1:(nrow(tmp)-5),]
  GeneName = unlist(tmp[,1])
  M = matrix(NA,nrow=length(GeneName),ncol=length(sub_dirs));rm(tmp)
  stat = matrix(NA,nrow=5,ncol=length(sub_dirs))
  for(i in seq_along(sub_dirs)) {
    tmp = read.delim(file.path(sub_dirs[i],readscountFile[i]),header=F)
    stati = ( tmp[(nrow(tmp)-4):nrow(tmp),2])
    stat[,i]  =stati
    tmp = tmp[1:(nrow(tmp)-5),]
    M[,i] = (tmp[,2])
  }
  rownames(M) = GeneName; colnames(M) = sampleID
  rownames(stat) = c('__no_feature',
                     '__ambiguous',
                     '__to_low_aQual',
                     '__not_aligned',
                     '__alignment_not_unique')
  colnames(stat) = sampleID
  return(list(M=M,
              stat=stat))
}

##sample id that wiil be used
##dir name to sample id
sub_dirs = list.dirs(htseq_root_dir,recursive=F)
dirs_name = basename(sub_dirs) 
##sample info is not fully populated now
sample_ID = dirs_name


##match(sample_ID, sampleinfo$newname1) -> pos
##sampleinfo = sampleinfo[pos,]
htseqObj = combineHTseq(htseq_root_dir,sample_ID)
gff_gene = filter(gff, type == 'gene')
gene_annotation = gff_gene[match(rownames(htseqObj$M),gff_gene$gene_id),]
countMtr = tbl_df(data.frame(
  SYMBOL=gene_annotation$gene_name,
  ID=gene_annotation$gene_id,
  Type=gene_annotation$gene_type,
  htseqObj$M,
  check.names = F
))
##get gene length Matrix
gff_exon = dplyr::filter(gff, type == 'exon')
##take the overlapped part length of exons of the same gene
##this gff file has all end >= start ...
myExonSum = function(gene.exon.gff) {
  start = min(gene.exon.gff$start)
  end = max(gene.exon.gff$end)
  len = end-start + 1
  tmp = rep(F, len)
  tmpstart = gene.exon.gff$start - start + 1
  tmpend = tmpstart + (gene.exon.gff$end - gene.exon.gff$start)
  for(i in seq_along(tmpstart)) {
    tmp[tmpstart[i]:tmpend[i]] = T
  }
  return(data.frame(sum(tmp)))
}
geneLen = gff_exon %>%
  group_by(gene_id) %>%
  do(myExonSum(gene.exon.gff = .)) %>%
  ungroup 
colnames(geneLen)[2] = 'GeneLen'
match(gene_annotation$gene_id, geneLen$gene_id ) -> pos
geneLen = geneLen[pos,]
geneLenMtr = tbl_df(data.frame(
  SYMBOL=gene_annotation$gene_name,
  ID=gene_annotation$gene_id,
  Type=gene_annotation$gene_type,
  geneLength=geneLen$GeneLen,
  check.names = F))

pcg_countMtr = filter(countMtr, Type == 'protein_coding')
nonpcg_countMtr = filter(countMtr, Type != 'protein_coding')
pcg_geneLen = filter(geneLenMtr, Type == 'protein_coding')
nonpcg_geneLen = filter(geneLenMtr, Type != 'protein_coding')
##lncrna pseudogene definition based on:
##1. Higher level class definition according to : ftp://ftp.sanger.ac.uk/pub/gencode/_README_stats.txt
##2. https://www.gencodegenes.org/gencode_biotypes.html
##3. hg19 don't have all of these, use those existing ones
lncrna_countMtr = filter(countMtr, Type  %in% c(
                                                '3prime_overlapping_ncrna',
                                                'antisense',
                                                'lincRNA',
                                                'processed_transcript',
                                                'sense_intronic',
                                                'sense_overlapping',
                                                ##'TEC',
                                                ##                                               'protein_coding',
                                                'polymorphic_pseudogene',
                                                'processed_pseudogene',
                                                'pseudogene'
                                              ))
lncrna_geneLen = filter(geneLenMtr, Type %in% c(
                                                '3prime_overlapping_ncrna',
                                                'antisense',
                                                'lincRNA',
                                                'processed_transcript',
                                                'sense_intronic',
                                                'sense_overlapping',
                                                ##'TEC',
                                                ##                                                'protein_coding',
                                                'polymorphic_pseudogene',
                                                'pseudogene'
                                              ))
write_tsv(countMtr, path=file.path(output_dir,'allgenes_countMatrix.tsv'))
write_tsv(geneLenMtr, path=file.path(output_dir,'allgenes_geneLengthMatrix.tsv'))
write_tsv(pcg_countMtr, path=file.path(output_dir,'protein_coding_gene_countMatrix.tsv'))
write_tsv(nonpcg_countMtr, path=file.path(output_dir,'Non_protein_coding_gene_countMatrix.tsv'))
write_tsv(lncrna_countMtr, path=file.path(output_dir,'lncrna_pseudogene_gene_countMatrix.tsv'))
write_tsv(pcg_geneLen, path=file.path(output_dir,'protein_coding_gene_geneLengthMatrix.tsv'))
write_tsv(nonpcg_geneLen, path=file.path(output_dir,'Non_protein_coding_gene_geneLengthMatrix.tsv'))
write_tsv(lncrna_geneLen, path=file.path(output_dir,'lncrna_pseudogene_geneLengthMatrix.tsv'))


##calculate FPKM and FPKM-uq(https://docs.gdc.cancer.gov/Data/Bioinformatics_Pipelines/Expression_mRNA_Pipeline/#data-processing-steps)
myfpkm = function(RCg,RCpc,L) RCg*10e9/(RCpc*L)
myfpkmuq = function(RCg,RCg75,L) RCg*10e9/(RCg75*L)

##RCg: Number of reads mapped to the gene
##RCpc: Number of reads mapped to all protein-coding genes
##RCg75: The 75th percentile read count value for genes in the sample
##L: Length of the gene in base pairs; Calculated as the sum of all exons in a gene
pcg_countMtr1 = select(pcg_countMtr, -SYMBOL,
                  -ID,-Type)
geneLen = unlist(pcg_geneLen$geneLength) ##L
libSz = apply(pcg_countMtr1, 2, sum) ##RCpc 
fpkm = t(apply(pcg_countMtr1*1e9,1,function(rw,libSz) rw/libSz,
               libSz = libSz))
fpkm = apply(fpkm, 2, function(cl,geneLen) cl/geneLen,
             geneLen = geneLen)
fpkm = tbl_df(data.frame(
  select(pcg_countMtr, SYMBOL, ID, Type),
  fpkm,
  check.names =F
))
write_tsv(fpkm, path=file.path(output_dir,'protein_coding_gene_fpkm.tsv'))

##fpkm with edger
library(edgeR)
fpkm.edger = rpkm(pcg_countMtr1, gene.length = geneLen)
fpkm.edger = tbl_df(data.frame(
  select(pcg_countMtr, SYMBOL, ID, Type),
  fpkm.edger,
  check.names = F
))
##produce the same result...
write_tsv(fpkm.edger, path=file.path(output_dir,'protein_coding_gene_fpkm_edger.tsv'))

##fpkm for all together 
allgenes.countMtr1 = select(countMtr, -SYMBOL, -ID, -Type)
geneLen = unlist(geneLenMtr$geneLength)
allgenes.fpkm.edger = rpkm(allgenes.countMtr1, gene.length = geneLen)
allgenes.fpkm.edger = tbl_df(data.frame(
  select(countMtr, SYMBOL, ID, Type),
  allgenes.fpkm.edger,
  check.names = F
))
##produce the same result...
write_tsv(allgenes.fpkm.edger, path=file.path(output_dir,'all_genes_fpkm_edger.tsv'))

libSz = apply(allgenes.countMtr1, 2, sum) ##RCpc 
allgenes.fpkm1 = t(apply(allgenes.countMtr1*1e9,1,function(rw,libSz) rw/libSz,
               libSz = libSz))
allgenes.fpkm1 = apply(allgenes.fpkm1, 2, function(cl,geneLen) cl/geneLen,
             geneLen = geneLen)
allgenes.fpkm1 = tbl_df(data.frame(
  select(countMtr, SYMBOL, ID, Type),
  allgenes.fpkm1,
  check.names =F
))

##produce the same result...
write_tsv(allgenes.fpkm1, path=file.path(output_dir,'all_genes_fpkm.tsv'))




##want a correlation heatmap plot?
gene.df = select(pcg_countMtr, SYMBOL,ID)
dat.df = select(pcg_countMtr, -SYMBOL, -ID,-Type) 
##remove zero
low.idx = rowSums(dat.df) == 0
dat.df = dat.df[!low.idx,]
gene.df = gene.df[!low.idx,]
corrSamples = cor(dat.df,method = 'pearson')
library(pheatmap)
library(RColorBrewer)
pheatmap(mat=corrSamples,
         ##          color = colorRampPalette((brewer.pal(n = 7, name = "RdYlBu")))(100),
         ##         cluster_cols=F,
         ##         show_colnames = F,
         border_color = 'white',
         show_colnames=F,
         filename=file.path(output_dir,'sampleCorrelation.heatmap.pdf'))

