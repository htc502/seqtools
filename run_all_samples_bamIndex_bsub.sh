####################################################
# samtools index : for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/sh

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create bamIndex_bam_sample_scripts subdir in $SCRIPTS_PATH..."
    bamIndex_sample_scripts_path=${SCRIPTS_PATH}/bamIndex_bam_sample_scripts
    mkdir -p ${bamIndex_sample_scripts_path}
    BAMINDEX_BAM_LIST=$(cat $configFile | grep -w 'BAMINDEX_BAM_LIST' | cut -d '=' -f2)
    echo "looping through each bam file..."
    cat $BAMINDEX_BAM_LIST | while read line; do
        echo "$line"
        BAM=$(basename "${line}")
        BAM=${BAM/.bam/}
        let c=c+1
        echo "#BSUB -J bamIndex_${BAM}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/bamIndex_${BAM}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/bamIndex_${BAM}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N

module load samtools/1.1

sh ${SCRIPTS_PATH}/bamIndex_for_one_sample.sh $line ${SCRIPTS_PATH}/config/tool_info.txt " > ${bamIndex_sample_scripts_path}/bamIndex_${BAM}.lsf
       bsub< ${bamIndex_sample_scripts_path}/bamIndex_${BAM}.lsf
        sleep 1
    done
fi
