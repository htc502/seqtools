####################################################
# mirdeep2 pipeline for one sample
# inputs: 
#    samplepath
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

if [ $# != 2 ]; then ## $# doesn't count the script name
    echo " usage: samplepath configFile";
else
    samplefilepath=$1
    configFile=$2
    fastq=0 ##fqinput
    mapper_params=0 ##read from config
    bowtie_ref_genome=0 ##reference genome
    output_dir=0 
    nthread=0
    sample=0 ##samplename for output
    mirbase_mature_seqs=0
    cwd=$(basename $fastq)
    cd $cwd
    gzip -d $fastq
    newfq=${fastq/.gz/}
    mapper.pl $newfq $mapper_params -p $bowtie_ref_genome -s $output_dir/$sample.reads.fa -t $output_dir/$sample.reads_vs_genome.arf -o $nthread
    rm $newfq
    miRDeep2.pl $output_dir/$sample.reads.fa $ref_genome $output_dir/$sample.reads_vs_genome.arf $mirbase_mature_seqs $close_species $mirbase_precursor_seqs $mirdeep2_params 
fi
