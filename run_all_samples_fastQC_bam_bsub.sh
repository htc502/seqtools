####################################################
# rsem+star pipeline: fastQC for all samples in a file
# inputs: 
#    configFile(a configuration file includes those parameters needed by individual tool,i.e. bowtie, star, rsem etc)
####################################################

#!/bin/bash

if [ $# != 1 ]; then ## $# doesn't count the script name
    echo " usage: configFile";
else
    c=0
    echo "$c"
    configFile=$1
    SCRIPTS_PATH=$(cat $configFile | grep -w 'SCRIPTS_PATH' | cut -d '=' -f2)
    echo "create fastQC_bam_sample_scripts subdir in $SCRIPTS_PATH..."
    fastQC_sample_scripts_path=${SCRIPTS_PATH}/fastQC_bam_sample_scripts
    mkdir -p ${fastQC_sample_scripts_path}
    BAM_LIST=$(cat $configFile | grep -w 'BAM_LIST' | cut -d '=' -f2)
    echo "looping through each bam file..."
    cat $BAM_LIST | while read line; do
        echo "$line"
        BAM=${line/.bam/}
        let c=c+1
        echo "#BSUB -J fqc_${BAM}
#BSUB -W 10:00
#BSUB -oo /rsrch2/genomic_med/ghan1/log/fqc_${BAM}.o 
#BSUB -eo /rsrch2/genomic_med/ghan1/log/fqc_${BAM}.e 
#BSUB -q medium
#BSUB -n  2
#BSUB -M 17408
#BSUB -R rusage[mem=17408]
#BSUB -B
#BSUB -N
#BSUB -u ghan1@mdanderson.org

module load fastqc/0.11.5

sh ${SCRIPTS_PATH}/fastqc_bam_for_one_sample.sh $line ${SCRIPTS_PATH}/config/tool_info.txt " > ${fastQC_sample_scripts_path}/fastQC_bam_${BAM}.lsf
        bsub< ${fastQC_sample_scripts_path}/fastQC_bam_${BAM}.lsf
        sleep 2
    done
fi
