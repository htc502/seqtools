####################################################
# salmon for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile
####################################################

#!/bin/sh
if [ $# != 3 ]; then
    echo " usage: samplefq1 samplefq2 configFile";
else
    fq1=$1
    fq2=$2
    configFile=$3
    salmon_bin=$(cat $configFile | grep -w 'SALMON_BIN' | cut -d '=' -f2)
    salmon_index=$(cat $configFile | grep -w 'SALMON_INDEX' | cut -d '=' -f2)
    nthread=$(cat $configFile | grep -w 'SALMON_NP' | cut -d '=' -f2)
    outputdir=$(cat $configFile | grep -w 'SALMON_OUTPUT_DIR' | cut -d '=' -f2)
    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R1*gz/}
    ##-l A salmon will automatically determine the library type
    ${salmon_bin} quant -i $salmon_index -l A 	-1 $fq1	-2 $fq2 --numBootstraps 1000 -p $nthread -o $outputdir/$samplename
fi
