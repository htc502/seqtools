scriptDir=commandArgs(trailingOnly=T)
if(!dir.exists(scriptDir)) stop('error: loading scriptDir')
##want to extract variables from those scripts
configFile = file.path(scriptDir,'config/tool_info.txt')
##backup if already exists
if(file.exists(configFile)) file.rename(configFile,file.path(scriptDir,'config/tool_info.txt.bk'))

scripts = list.files(path = scriptDir, pattern = '\\.r$|\\.sh$',
                     include.dirs = F)
pt = "\\| grep -w \\'(.*)\\' \\|"
library(stringr)
res = list()
for( i in seq_along(scripts)) {
  scripti = scripts[i]
  tmpi = readLines(file.path(scriptDir,scripti))
  tmpii = str_match(tmpi,pt)[,2]
  tmpii = trimws(na.omit(tmpii))
  if(length(tmpii)==0) {
    next
  } else {
  res[[length(res)+1]] = tmpii
  names(res)[length(res)] = scripti
  }
}
##unique and sort them
res1 = unique(sort(unlist(res)))
##move script_path first place
idx=which(res1=='SCRIPTS_PATH')
res1=res1[-idx]
res1=c('SCRIPTS_PATH',res1)
configTemplate=file.path(scriptDir,'config/tool_info.txt')
write('##config template\n',file = configTemplate)

for(i in seq_along(res1)) {
  write(paste0(res1[i],'='),file = configTemplate,append = T)
}
