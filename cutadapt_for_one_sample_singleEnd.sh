####################################################
# rsem+star pipeline for one sample
# inputs: 
#    sampleName(ie. SRR34567_R1    note that file suffix is not included)
#    configFile(a configuration file includes those parameters needed by mirdeep2 and bowtie)
####################################################

if [ $# != 2 ]; then
    echo " usage: sample.fq configFile";
else
    fq1=$1
    samplename=`basename $fq1`
    samplename=${samplename/[.|_]R1*gz/}
    configFile=$2

    output_dir=$(cat $configFile | grep -w 'CUTADAPT_OUTPUT_PATH' | cut -d '=' -f2)
    cutadapt_params=$(cat $configFile | grep -w 'CUTADAPT_PARAMS' | cut -d '=' -f2)

    mkdir -p  $output_dir
    cd $output_dir
    module load python/2.7.6-2-anaconda
    module load cutadapt/1.8.1
    echo "cut adaptors and trim low quality bases"
    cutadapt $cutadapt_params  -o $output_dir/${samplename}_R1.fastq $fq1> $output_dir/$samplename.cutadapt.log
fi
